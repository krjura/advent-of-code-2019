package org.krjura.adventofcode.day10.pojo;

import java.util.List;
import java.util.Objects;

public class DestroyResult {

    private final Coordinate monitoringStation;

    private final List<CoordinateWithAngle> destroyed;

    private DestroyResult(Coordinate monitoringStation, List<CoordinateWithAngle> destroyed) {
        this.monitoringStation = Objects.requireNonNull(monitoringStation);
        this.destroyed = Objects.requireNonNull(destroyed);
    }

    public static DestroyResult of(Coordinate monitoringStation, List<CoordinateWithAngle> destroyed) {
        return new DestroyResult(monitoringStation, destroyed);
    }

    public Coordinate getMonitoringStation() {
        return monitoringStation;
    }

    public List<CoordinateWithAngle> getDestroyed() {
        return destroyed;
    }
}
