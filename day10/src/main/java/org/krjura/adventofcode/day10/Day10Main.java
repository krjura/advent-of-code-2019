package org.krjura.adventofcode.day10;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.krjura.adventofcode.day10.pojo.Coordinate;
import org.krjura.adventofcode.day10.pojo.CoordinateWithAngle;
import org.krjura.adventofcode.day10.pojo.CoordinateWithVisible;
import org.krjura.adventofcode.day10.pojo.DestroyResult;

import static java.lang.String.format;
import static java.util.Comparator.comparingDouble;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toMap;
import static org.krjura.adventofcode.day10.utils.LineUtils.calculateAngle;
import static org.krjura.adventofcode.day10.utils.LineUtils.onLine;
import static org.krjura.adventofcode.day10.utils.WorldUtils.initializeWorld;

public class Day10Main {

    private final World world;

    public Day10Main(Path inputPath) {
        this.world = initializeWorld(inputPath);
    }

    public DestroyResult destroy() {
        var currentWorld = world.copy();

        var destroyed = new ArrayList<CoordinateWithAngle>(currentWorld.getAllCoordinates().size());

        // find best initial spot
        Coordinate monitoringStation = calculateMonitoringStationLocation(currentWorld);
        currentWorld.remove(monitoringStation); // remove from world

        while (currentWorld.getAllCoordinates().size() != 0) {

            var visibleAsteroids = calculateVisibilityOfAsteroids(currentWorld.getAllCoordinates(), monitoringStation);
            // sort by angles
            var sortedByAngle = visibleAsteroids
                    .stream()
                    .map(current -> calculateAngle(monitoringStation, current))
                    .sorted(comparingDouble(CoordinateWithAngle::getAngle))
                    .collect(Collectors.toList());

            // add sorted to result list
            destroyed.addAll(sortedByAngle);

            // remove from world since they are now destroy
            sortedByAngle.forEach(it -> currentWorld.remove(it.getCoordinate()));
        };

        return DestroyResult.of(monitoringStation, destroyed);
    }

    private Coordinate calculateMonitoringStationLocation(World currentWorld) {
        return findAsteroidWithMaxVisibility(calculateVisibilityOfAsteroids(currentWorld))
                .orElseThrow(() -> new RuntimeException("no max found"))
                .getCoordinate();
    }

    public Optional<CoordinateWithVisible> findAsteroidWithMaxVisibility() {
        return findAsteroidWithMaxVisibility(calculateVisibilityOfAsteroids());
    }

    private Optional<CoordinateWithVisible> findAsteroidWithMaxVisibility(Map<Coordinate, CoordinateWithVisible> visibility) {
        return visibility
                .values()
                .stream()
                .max(comparingInt(CoordinateWithVisible::getCount));
    }

    public Map<Coordinate, CoordinateWithVisible> calculateVisibilityOfAsteroids() {
        return calculateVisibilityOfAsteroids(this.world);
    }

    private Map<Coordinate, CoordinateWithVisible> calculateVisibilityOfAsteroids(World actualWorld) {
        var coordinates = actualWorld.getAllCoordinates();

        return coordinates
                .stream()
                .map(current -> CoordinateWithVisible.of(current, calculateVisibilityOfAsteroids(coordinates, current)))
                .collect(toMap(CoordinateWithVisible::getCoordinate, Function.identity()));
    }

    private List<Coordinate> calculateVisibilityOfAsteroids(Set<Coordinate> allAsteroids, Coordinate current) {

        Predicate<Coordinate> notCurrent = other -> ! current.equals(other);
        Predicate<Coordinate> notBlocked = other -> ! isBlocked(allAsteroids, current, other);

        return allAsteroids
                .stream()
                .filter(notCurrent)
                .filter(notBlocked)
                .collect(Collectors.toList());
    }

    public void printVisibilityMap(Map<Coordinate, CoordinateWithVisible> visibility) {

        int maxY = this.world.getMaxY();
        int maxX = this.world.getMaxX();

        for(int j = 0; j <= maxY; j++) {
            System.out.println("");

            for(int i = 0; i <= maxX; i++) {
                var coordinate = Coordinate.of(i, j);
                var info = visibility.getOrDefault(coordinate, CoordinateWithVisible.of(coordinate, List.of()));

                System.out.print(format("%3d", info.getCount()));
            }
        }
    }

    private boolean isBlocked(Set<Coordinate> coordinates, Coordinate pl1, Coordinate pl2) {
        var exclude = List.of(pl1, pl2);

        Predicate<Coordinate> notProcessed = it -> ! exclude.contains(it);
        Predicate<Coordinate> betweenPoints = current -> onLine(pl1, pl2, current);

        return coordinates
                .stream()
                .filter(notProcessed)
                .anyMatch(betweenPoints);
    }

    public static void main(String[] args) {
        var main = new Day10Main(Path.of("day10-input.txt"));
        main.world.printWorld();

        System.out.println("\n");
        // main.calculateVisibility();

        //System.out.println(main.findWithMaxVisibility());
        main.destroy();
    }
}
