package org.krjura.adventofcode.day10;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.krjura.adventofcode.day10.pojo.Coordinate;

public class World {

    private final Map<Coordinate, EntityType> data;

    private int maxX = Integer.MIN_VALUE;

    private int maxY = Integer.MIN_VALUE;

    public World() {
        this.data = new HashMap<>();
    }

    private World(Map<Coordinate, EntityType> data, int maxX, int maxY) {
        this.data = data;
        this.maxX = maxX;
        this.maxY = maxY;
    }

    public void put(Coordinate coordinate, EntityType entityType) {
        this.data.put(coordinate, entityType);
        this.maxX = Math.max(coordinate.x, maxX);
        this.maxY = Math.max(coordinate.y, maxY);
    }

    public Optional<EntityType> get(Coordinate coordinate) {
        return Optional.ofNullable(this.data.get(coordinate));
    }

    public void remove(Coordinate val) {
        this.data.remove(val);
    }

    public Set<Coordinate> getAllCoordinates() {
        return Collections.unmodifiableSet(this.data.keySet());
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void printWorld() {
        for(int j = 0; j <= maxY; j++) {
            System.out.println("");

            for(int i = 0; i <= maxX; i++) {
                var type = get(Coordinate.of(i, j)).orElse(EntityType.NONE);

                switch (type) {
                    case NONE:
                        System.out.print(".");
                        break;
                    case ASTEROID:
                        System.out.print("#");
                        break;
                }
            }
        }
    }

    public World copy() {
        Map<Coordinate, EntityType> dataCopy = new HashMap<>(this.data.size());
        dataCopy.putAll(this.data);

        return new World(dataCopy, this.maxX, this.maxY);
    }
}
