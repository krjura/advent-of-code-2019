package org.krjura.adventofcode.day10.pojo;

import java.util.List;

public class CoordinateWithVisible {

    private final Coordinate coordinate;

    private final List<Coordinate> visibleAsteroids;

    private CoordinateWithVisible(Coordinate coordinate, List<Coordinate> visibleAsteroids) {
        this.coordinate = coordinate;
        this.visibleAsteroids = visibleAsteroids;
    }

    public static CoordinateWithVisible of(Coordinate coordinate, List<Coordinate> visibleAsteroids) {
        return new CoordinateWithVisible(coordinate, visibleAsteroids);
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public List<Coordinate> getVisibleAsteroids() {
        return visibleAsteroids;
    }

    public int getCount() {
        return this.visibleAsteroids.size();
    }

    @Override
    public String toString() {
        return "CoordinateWithVisible{" +
                "coordinate=" + coordinate +
                ", visibleAsteroids=" + visibleAsteroids +
                '}';
    }
}
