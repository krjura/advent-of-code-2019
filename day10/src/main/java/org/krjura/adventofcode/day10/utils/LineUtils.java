package org.krjura.adventofcode.day10.utils;

import org.krjura.adventofcode.day10.pojo.Coordinate;
import org.krjura.adventofcode.day10.pojo.CoordinateWithAngle;

import static java.lang.Math.PI;
import static java.lang.Math.atan;

public class LineUtils {

    private LineUtils() {
        // util
    }

    public static boolean collinear(Coordinate lp1, Coordinate lp2, Coordinate p)
    {
        if ((p.y - lp2.y) * (lp2.x - lp1.x) == (lp2.y - lp1.y) * (p.x - lp2.x)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean onLine(Coordinate lp1, Coordinate lp2, Coordinate p) {
        // first check if p is between lp1 and lp2
        if( lp1.x < lp2.x && (p.x < lp1.x || p.x > lp2.x)) {
            return false;
        } else if( lp1.x > lp2.x && (p.x > lp1.x || p.x < lp2.x) ) {
            return false;
        } else if( lp1.y < lp2.y && (p.y < lp1.y || p.y > lp2.y)) {
            return false;
        } else if( lp1.y > lp2.y && (p.y > lp1.y || p.y < lp2.y) ) {
            return false;
        }

        return collinear(lp1, lp2, p);
    }

    public static CoordinateWithAngle calculateAngle(Coordinate center, Coordinate other) {
        // sanity check
        if(center.x < 0 || center.y < 0 || other.x < 0 || other.y < 0) {
            throw new IllegalArgumentException(String.format("points %s, %s cannot be negative", center, other));
        } else if(center.equals(other)) {
            throw new IllegalArgumentException("points are the same: " + center);
        }

        // 4 | 1
        // 3 | 2
        int centerY = - center.y;
        int centerX = center.x;

        int otherY = - other.y;
        int otherX = other.x;

        // slope
        double m2 = ((double) ( otherY - centerY )) / ( (double) (otherX - centerX ));

        if(centerX == otherX && centerY < otherY) {
            return CoordinateWithAngle.of(other, 0.0d);
        } else if(centerY == otherY && centerX < otherX) {
            return CoordinateWithAngle.of(other, 90.0d);
        } else if(centerX == otherX && centerY > otherY) {
            return CoordinateWithAngle.of(other, 180.0d);
        } else if(centerY == otherY && centerX > otherX) {
            return CoordinateWithAngle.of(other, 270.0d);
            // 1 quadrant
        } else if(centerY < otherY && centerX < otherX) {
            var angle = 90 - (atan(m2) * ( 180.0d / PI));
            return CoordinateWithAngle.of(other, angle);
            // 2 quadrant
        } else if(centerY > otherY && centerX < otherX) {
            var angle = 90 - ( atan(m2) * ( 180.0d / PI) );
            return CoordinateWithAngle.of(other, angle);
            // 4 quadrant
        } else if(centerY < otherY && centerX > otherX) {
            var angle = 270.0d - ( atan(m2) * ( 180.0d / PI) );
            return CoordinateWithAngle.of(other, angle);
            // 3 quadrant
        } else if(centerY > otherY && centerX > otherX) {
            var angle = 270.0d - ( atan(m2) * ( 180.0d / PI) );
            return CoordinateWithAngle.of(other, angle);
        } else {
            throw new RuntimeException("should not have happened");
        }
    }
}
