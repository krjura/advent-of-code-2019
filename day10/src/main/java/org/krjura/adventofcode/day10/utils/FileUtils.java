package org.krjura.adventofcode.day10.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class FileUtils {

    private FileUtils() {
        // utils
    }

    public static List<String> readAllLines(Path inputPath) {
        try {
            return Files.readAllLines(inputPath);
        } catch (IOException e) {
            throw new RuntimeException("cannot read lines", e);
        }
    }
}