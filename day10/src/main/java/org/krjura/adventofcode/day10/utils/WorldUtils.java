package org.krjura.adventofcode.day10.utils;

import java.nio.file.Path;
import java.util.List;
import org.krjura.adventofcode.day10.EntityType;
import org.krjura.adventofcode.day10.World;
import org.krjura.adventofcode.day10.pojo.Coordinate;

import static org.krjura.adventofcode.day10.utils.FileUtils.readAllLines;

public class WorldUtils {

    private WorldUtils() {
        // utils
    }

    public static World initializeWorld(Path inputPath) {

        World newWorld = new World();
        List<String> lines = readAllLines(inputPath);

        for(int j = 0, jn = lines.size(); j < jn; j++) {

            String line = lines.get(j);

            char[] elements = line.toCharArray();

            for(int i = 0, in = elements.length; i < in; i++) {
                char element = elements[i];

                if(element == '#') {
                    newWorld.put(Coordinate.of(i, j), EntityType.ASTEROID);
                }
            }
        }

        return newWorld;
    }
}
