package org.krjura.adventofcode.day10.pojo;

public class CoordinateWithAngle {

    private final Coordinate coordinate;

    private final double angle;

    private CoordinateWithAngle(Coordinate coordinate, double angle) {
        this.coordinate = coordinate;
        this.angle = angle;
    }

    public static CoordinateWithAngle of(Coordinate coordinate, double angle) {
        return new CoordinateWithAngle(coordinate, angle);
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public double getAngle() {
        return angle;
    }
}
