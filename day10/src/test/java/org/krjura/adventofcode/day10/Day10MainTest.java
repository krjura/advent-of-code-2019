package org.krjura.adventofcode.day10;

import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.day10.pojo.Coordinate;
import org.krjura.adventofcode.day10.pojo.CoordinateWithVisible;

import java.nio.file.Path;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class Day10MainTest {

    @Test
    public void testExample1Visibility() {
        var main = new Day10Main(Path.of("day10-part1-example1.txt"));
        var visibility = main.calculateVisibilityOfAsteroids();

        assertThat(getCount(visibility, 0, 0)).isNull();
        assertThat(getCount(visibility, 1, 0)).isEqualTo(7);
        assertThat(getCount(visibility, 2, 0)).isNull();
        assertThat(getCount(visibility, 3, 0)).isNull();
        assertThat(getCount(visibility, 4, 0)).isEqualTo(7);

        assertThat(getCount(visibility, 0, 1)).isNull();
        assertThat(getCount(visibility, 1, 1)).isNull();
        assertThat(getCount(visibility, 2, 1)).isNull();
        assertThat(getCount(visibility, 3, 1)).isNull();
        assertThat(getCount(visibility, 4, 1)).isNull();

        assertThat(getCount(visibility, 0, 2)).isEqualTo(6);
        assertThat(getCount(visibility, 1, 2)).isEqualTo(7);
        assertThat(getCount(visibility, 2, 2)).isEqualTo(7);
        assertThat(getCount(visibility, 3, 2)).isEqualTo(7);
        assertThat(getCount(visibility, 4, 2)).isEqualTo(5);

        assertThat(getCount(visibility, 0, 3)).isNull();
        assertThat(getCount(visibility, 1, 3)).isNull();
        assertThat(getCount(visibility, 2, 3)).isNull();
        assertThat(getCount(visibility, 3, 3)).isNull();
        assertThat(getCount(visibility, 4, 4)).isEqualTo(7);

        assertThat(getCount(visibility, 0, 4)).isNull();
        assertThat(getCount(visibility, 1, 4)).isNull();
        assertThat(getCount(visibility, 2, 4)).isNull();
        assertThat(getCount(visibility, 3, 4)).isEqualTo(8);
        assertThat(getCount(visibility, 4, 4)).isEqualTo(7);
    }

    @Test
    public void testExample1Result() {
        var main = new Day10Main(Path.of("day10-part1-example1.txt"));
        var max = main.findAsteroidWithMaxVisibility();

        assertThat(max).isPresent();
        assertThat(max.get().getCoordinate()).isEqualTo(Coordinate.of(3, 4));
        assertThat(max.get().getCount()).isEqualTo(8);
    }

    @Test
    public void testExample2Result() {
        var main = new Day10Main(Path.of("day10-part1-example2.txt"));
        var max = main.findAsteroidWithMaxVisibility();

        assertThat(max).isPresent();
        assertThat(max.get().getCoordinate()).isEqualTo(Coordinate.of(5, 8));
        assertThat(max.get().getCount()).isEqualTo(33);
    }

    @Test
    public void testExample3Result() {
        var main = new Day10Main(Path.of("day10-part1-example3.txt"));
        var max = main.findAsteroidWithMaxVisibility();

        assertThat(max).isPresent();
        assertThat(max.get().getCoordinate()).isEqualTo(Coordinate.of(1, 2));
        assertThat(max.get().getCount()).isEqualTo(35);
    }

    @Test
    public void testExample4Result() {
        var main = new Day10Main(Path.of("day10-part1-example4.txt"));
        var max = main.findAsteroidWithMaxVisibility();

        assertThat(max).isPresent();
        assertThat(max.get().getCoordinate()).isEqualTo(Coordinate.of(6, 3));
        assertThat(max.get().getCount()).isEqualTo(41);
    }

    @Test
    public void testExample5Result() {
        var main = new Day10Main(Path.of("day10-part1-example5.txt"));
        var max = main.findAsteroidWithMaxVisibility();

        assertThat(max).isPresent();
        assertThat(max.get().getCoordinate()).isEqualTo(Coordinate.of(11, 13));
        assertThat(max.get().getCount()).isEqualTo(210);
    }

    @Test
    public void testInputResult() {
        var main = new Day10Main(Path.of("day10-input.txt"));
        var max = main.findAsteroidWithMaxVisibility();

        assertThat(max).isPresent();
        assertThat(max.get().getCoordinate()).isEqualTo(Coordinate.of(14, 17));
        assertThat(max.get().getCount()).isEqualTo(260);
    }

    @Test
    public void testDestroyedExample1() {
        var main = new Day10Main(Path.of("day10-part2-example1.txt"));

        var result = main.destroy();

        assertThat(result.getMonitoringStation()).isEqualTo(Coordinate.of(8, 3));
        assertThat(result.getDestroyed().get(0).getCoordinate()).isEqualTo(Coordinate.of(8, 1));
        assertThat(result.getDestroyed().get(1).getCoordinate()).isEqualTo(Coordinate.of(9, 0));
        assertThat(result.getDestroyed().get(2).getCoordinate()).isEqualTo(Coordinate.of(9, 1));
        assertThat(result.getDestroyed().get(3).getCoordinate()).isEqualTo(Coordinate.of(10, 0));
        assertThat(result.getDestroyed().get(4).getCoordinate()).isEqualTo(Coordinate.of(9, 2));
        assertThat(result.getDestroyed().get(5).getCoordinate()).isEqualTo(Coordinate.of(11, 1));
        assertThat(result.getDestroyed().get(6).getCoordinate()).isEqualTo(Coordinate.of(12, 1));
        assertThat(result.getDestroyed().get(7).getCoordinate()).isEqualTo(Coordinate.of(11, 2));
        assertThat(result.getDestroyed().get(8).getCoordinate()).isEqualTo(Coordinate.of(15, 1));
        assertThat(result.getDestroyed().get(9).getCoordinate()).isEqualTo(Coordinate.of(12, 2));
        assertThat(result.getDestroyed().get(10).getCoordinate()).isEqualTo(Coordinate.of(13, 2));
        assertThat(result.getDestroyed().get(11).getCoordinate()).isEqualTo(Coordinate.of(14, 2));
        assertThat(result.getDestroyed().get(12).getCoordinate()).isEqualTo(Coordinate.of(15, 2));
        assertThat(result.getDestroyed().get(13).getCoordinate()).isEqualTo(Coordinate.of(12, 3));
        assertThat(result.getDestroyed().get(14).getCoordinate()).isEqualTo(Coordinate.of(16, 4));
        assertThat(result.getDestroyed().get(15).getCoordinate()).isEqualTo(Coordinate.of(15, 4));
        assertThat(result.getDestroyed().get(16).getCoordinate()).isEqualTo(Coordinate.of(10, 4));
        assertThat(result.getDestroyed().get(17).getCoordinate()).isEqualTo(Coordinate.of(4, 4));
        assertThat(result.getDestroyed().get(18).getCoordinate()).isEqualTo(Coordinate.of(2, 4));
        assertThat(result.getDestroyed().get(19).getCoordinate()).isEqualTo(Coordinate.of(2, 3));
        assertThat(result.getDestroyed().get(20).getCoordinate()).isEqualTo(Coordinate.of(0, 2));
        assertThat(result.getDestroyed().get(21).getCoordinate()).isEqualTo(Coordinate.of(1, 2));
        assertThat(result.getDestroyed().get(22).getCoordinate()).isEqualTo(Coordinate.of(0, 1));
        assertThat(result.getDestroyed().get(23).getCoordinate()).isEqualTo(Coordinate.of(1, 1));
        assertThat(result.getDestroyed().get(24).getCoordinate()).isEqualTo(Coordinate.of(5, 2));
        assertThat(result.getDestroyed().get(25).getCoordinate()).isEqualTo(Coordinate.of(1, 0));
        assertThat(result.getDestroyed().get(26).getCoordinate()).isEqualTo(Coordinate.of(5, 1));
        assertThat(result.getDestroyed().get(27).getCoordinate()).isEqualTo(Coordinate.of(6, 1));
        assertThat(result.getDestroyed().get(28).getCoordinate()).isEqualTo(Coordinate.of(6, 0));
        assertThat(result.getDestroyed().get(29).getCoordinate()).isEqualTo(Coordinate.of(7, 0));
        assertThat(result.getDestroyed().get(30).getCoordinate()).isEqualTo(Coordinate.of(8, 0));
        assertThat(result.getDestroyed().get(31).getCoordinate()).isEqualTo(Coordinate.of(10, 1));
        assertThat(result.getDestroyed().get(32).getCoordinate()).isEqualTo(Coordinate.of(14, 0));
        assertThat(result.getDestroyed().get(33).getCoordinate()).isEqualTo(Coordinate.of(16, 1));
        assertThat(result.getDestroyed().get(34).getCoordinate()).isEqualTo(Coordinate.of(13, 3));
        assertThat(result.getDestroyed().get(35).getCoordinate()).isEqualTo(Coordinate.of(14, 3));
    }

    @Test
    public void testDestroyedExample2() {
        var main = new Day10Main(Path.of("day10-part2-example2.txt"));

        var result = main.destroy();

        assertThat(result.getMonitoringStation()).isEqualTo(Coordinate.of(11, 13));
        assertThat(result.getDestroyed().get(0).getCoordinate()).isEqualTo(Coordinate.of(11, 12));
        assertThat(result.getDestroyed().get(1).getCoordinate()).isEqualTo(Coordinate.of(12, 1));
        assertThat(result.getDestroyed().get(2).getCoordinate()).isEqualTo(Coordinate.of(12, 2));
        assertThat(result.getDestroyed().get(9).getCoordinate()).isEqualTo(Coordinate.of(12, 8));
        assertThat(result.getDestroyed().get(19).getCoordinate()).isEqualTo(Coordinate.of(16, 0));
        assertThat(result.getDestroyed().get(49).getCoordinate()).isEqualTo(Coordinate.of(16, 9));
        assertThat(result.getDestroyed().get(99).getCoordinate()).isEqualTo(Coordinate.of(10, 16));
        assertThat(result.getDestroyed().get(198).getCoordinate()).isEqualTo(Coordinate.of(9, 6));
        assertThat(result.getDestroyed().get(199).getCoordinate()).isEqualTo(Coordinate.of(8, 2));
        assertThat(result.getDestroyed().get(200).getCoordinate()).isEqualTo(Coordinate.of(10, 9));
        assertThat(result.getDestroyed().get(298).getCoordinate()).isEqualTo(Coordinate.of(11, 1));
    }

    @Test
    public void testInput() {
        var main = new Day10Main(Path.of("day10-input.txt"));

        var result = main.destroy();

        var asteroid = result.getDestroyed().get(200 -1);
        var formula = asteroid.getCoordinate().x * 100 + asteroid.getCoordinate().y;

        assertThat(formula).isEqualTo(608);
    }

    private Integer getCount(Map<Coordinate, CoordinateWithVisible> visibility, int x, int y) {
        CoordinateWithVisible cc = visibility.get(Coordinate.of(x, y));

        return cc == null ? null : cc.getCount();
    }

}