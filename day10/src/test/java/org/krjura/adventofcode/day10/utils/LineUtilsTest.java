package org.krjura.adventofcode.day10.utils;

import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.day10.pojo.Coordinate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.adventofcode.day10.utils.LineUtils.calculateAngle;

public class LineUtilsTest {

    @Test
    public void pointIsOnLine() {
        assertThat(LineUtils.onLine(Coordinate.of(1, 1), Coordinate.of(3,3), Coordinate.of(2, 2))).isTrue();
        assertThat(LineUtils.onLine(Coordinate.of(0, 0), Coordinate.of(2,4), Coordinate.of(1, 2))).isTrue();
        assertThat(LineUtils.onLine(Coordinate.of(0, 0), Coordinate.of(2,4), Coordinate.of(2, 2))).isFalse();
        assertThat(LineUtils.onLine(Coordinate.of(0, 0), Coordinate.of(2,4), Coordinate.of(3, 6))).isFalse();
        assertThat(LineUtils.onLine(Coordinate.of(0, 0), Coordinate.of(2,4), Coordinate.of(-1, -1))).isFalse();
        assertThat(LineUtils.onLine(Coordinate.of(0, 0), Coordinate.of(2,4), Coordinate.of(0, 0))).isTrue();
        assertThat(LineUtils.onLine(Coordinate.of(0, 0), Coordinate.of(2,4), Coordinate.of(2, 4))).isTrue();
    }

    @Test
    public void angleBetweenLines() {
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(2, 0)).getAngle()).isEqualTo(0.0);
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(3, 1)).getAngle()).isEqualTo(45.0);
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(3, 2)).getAngle()).isEqualTo(90.0);
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(3, 3)).getAngle()).isEqualTo(135.0);
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(2, 3)).getAngle()).isEqualTo(180.0);
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(1, 3)).getAngle()).isEqualTo(225.0);
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(1, 2)).getAngle()).isEqualTo(270.0);
        assertThat(calculateAngle(Coordinate.of(2, 2), Coordinate.of(1, 1)).getAngle()).isEqualTo(315.0);
    }

}