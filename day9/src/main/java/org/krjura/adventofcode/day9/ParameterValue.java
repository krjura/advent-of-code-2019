package org.krjura.adventofcode.day9;

public class ParameterValue {

    private final int parameter;
    private final long position;
    private final int mode;
    private final long value;
    private final long valueAtPosition;

    private ParameterValue(int parameter, long position, int mode, long value, long valueAtPosition) {
        this.parameter = parameter;
        this.position = position;
        this.mode = mode;
        this.value = value;
        this.valueAtPosition = valueAtPosition;
    }

    private ParameterValue(Builder builder) {
        this(
                builder.parameter,
                builder.position,
                builder.mode,
                builder.value,
                builder.valueAtPosition
        );
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public int getParameter() {
        return parameter;
    }

    public long getPosition() {
        return position;
    }

    public long getMode() {
        return mode;
    }

    public long getValue() {
        return value;
    }

    public long getValueAtPosition() {
        return valueAtPosition;
    }

    public static final class Builder {
        private int parameter;
        private long position;
        private int mode;
        private long value;
        private long valueAtPosition;

        private Builder() {
        }

        public Builder parameter(int val) {
            parameter = val;
            return this;
        }

        public Builder position(long val) {
            position = val;
            return this;
        }

        public Builder mode(int val) {
            mode = val;
            return this;
        }

        public Builder value(long val) {
            value = val;
            return this;
        }

        public Builder valueAtPosition(long val) {
            valueAtPosition = val;
            return this;
        }

        public ParameterValue build() {
            return new ParameterValue(this);
        }
    }
}
