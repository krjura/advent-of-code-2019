package org.krjura.adventofcode.day9.providers;

public interface OutputDataProvider {

    void put(long data);
}
