package org.krjura.adventofcode.day9.providers;

public class SingleInputDataProvider implements InputDataProvider {

    private long data;

    public SingleInputDataProvider(long data) {
        this.data = data;
    }

    @Override
    public long data() {
        return data;
    }
}
