package org.krjura.adventofcode.day9.providers;

public class NoInputDataProvider implements InputDataProvider {

    @Override
    public long data() {
        throw new RuntimeException("no data available");
    }
}
