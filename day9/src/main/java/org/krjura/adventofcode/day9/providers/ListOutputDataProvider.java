package org.krjura.adventofcode.day9.providers;

import java.util.LinkedList;
import java.util.List;

public class ListOutputDataProvider implements OutputDataProvider {

    private List<Long> data;

    public ListOutputDataProvider() {
        this.data = new LinkedList<>();
    }

    @Override
    public void put(long data) {
        this.data.add(data);
    }

    public List<Long> getData() {
        return data;
    }

    public void clear() {
        data.clear();;
    }
}
