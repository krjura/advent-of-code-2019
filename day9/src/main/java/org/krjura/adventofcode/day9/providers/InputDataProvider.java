package org.krjura.adventofcode.day9.providers;

public interface InputDataProvider {

    long data();
}
