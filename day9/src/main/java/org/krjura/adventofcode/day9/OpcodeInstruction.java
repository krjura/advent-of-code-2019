package org.krjura.adventofcode.day9;

public class OpcodeInstruction {

    private final int[] instructions;
    private final int relativeBase;

    private OpcodeInstruction(long optCodeInstructions, int relativeBase) {
        this.instructions = processOpcodeInstruction(optCodeInstructions);
        this.relativeBase = relativeBase;
    }

    public static OpcodeInstruction of(long optCodeInstructions, int relativeBase) {
        return new OpcodeInstruction(optCodeInstructions, relativeBase);
    }

    private int[] processOpcodeInstruction(long opcode) {
        char[] instructionsChars = Long.toString(opcode).toCharArray();
        int[] instructionsInts = new int[instructionsChars.length];

        for(int i = 0, n = instructionsInts.length; i < n; i++) {
            instructionsInts[i] = Integer.parseInt(Character.toString(instructionsChars[i]));
        }

        return instructionsInts;
    }

    public int[] getInstructions() {
        return instructions;
    }

    public int getOpcode() {
        return getValueFromBack(1) * 10 + getValueFromBack(0);
    }

    public int getMode(int parameter) {
        if(parameter < 1) {
            throw new IllegalArgumentException("mode parameter must be > 1. Got " + parameter);
        }

        // +2 for opcode, (parameter -1) since we want parameter number to start from 1
        return getValueFromBack(2 + ( parameter - 1));
    }

    private int getValueFromBack(int position) {
        int index = this.instructions.length - position - 1; // since array start with 0

        if(index < 0) {
            return 0;
        } else if(index >= this.instructions.length ) {
            return 0;
        } else {
            return this.instructions[index];
        }
    }

    public ParameterValue getParameterValue(Memory memory, long position, int parameter) {
        long paramPosition = position + parameter;
        long value = memory.get(paramPosition);
        long realValue;

        int mode = getMode(parameter);

        if(mode == 0) {
            realValue = memory.get((int) value);
        } else if(mode == 1) {
            realValue = value;
        } else if(mode == 2) {
            realValue = memory.get(this.relativeBase + (int) value);
        } else {
            throw new IllegalArgumentException("unknown mode " + mode);
        }

        return ParameterValue
                .newBuilder()
                .parameter(parameter)
                .position(paramPosition)
                .mode(mode)
                .value(realValue)
                .valueAtPosition(value)
                .build();
    }

    public ParameterValue getParameterIndex(Memory memory, long position, int parameter) {
        long paramPosition = position + parameter;
        long value = memory.get(paramPosition);
        long realValue;

        int mode = getMode(parameter);

        if(mode == 0) {
            realValue = value;
        } else if(mode == 1) {
            realValue = paramPosition;
        } else if(mode == 2) {
            realValue = value + this.relativeBase;
        } else {
            throw new IllegalArgumentException("unknown mode " + mode);
        }

        return ParameterValue
                .newBuilder()
                .parameter(parameter)
                .position(paramPosition)
                .mode(0)
                .value(realValue)
                .valueAtPosition(value)
                .build();
    }
}
