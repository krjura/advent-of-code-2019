package org.krjura.adventofcode.day9;

import org.krjura.adventofcode.day9.providers.InputDataProvider;
import org.krjura.adventofcode.day9.providers.ListOutputDataProvider;
import org.krjura.adventofcode.day9.providers.OutputDataProvider;
import org.krjura.adventofcode.day9.providers.SingleInputDataProvider;

import static java.lang.String.format;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class Day9Main {

    private static final String CONST_ELEMENT_SEPARATOR = ",";

    private final String name;
    private final InputDataProvider inputDataProvider;
    private final OutputDataProvider outputDataProvider;
    private int relativeBase = 0;

    private Memory memory;

    private boolean logStages = true;

    public Day9Main(
            String name,
            Path inputPath,
            InputDataProvider inputDataProvider,
            OutputDataProvider outputDataProvider ) {

        this.memory = new Memory(convertToInt(readInputFile(inputPath).split(CONST_ELEMENT_SEPARATOR)));

        this.name = name;
        this.inputDataProvider = Objects.requireNonNull(inputDataProvider);
        this.outputDataProvider = Objects.requireNonNull(outputDataProvider);
    }

    public void disableLogging() {
        this.logStages = false;
    }

    private String readInputFile(Path inputPath) {
        try {
            return validateInput(Files.readAllLines(inputPath, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException("cannot read input file", e);
        }
    }

    private String validateInput(List<String> input) {
        if(input.size() > 1) {
            throw new IllegalArgumentException("got input with lines " + input.size() + " expecting 1");
        }

        return input.get(0);
    }

    public Memory calculate() {
        if(logStages) {
            System.out.println(format("[%s] starting calculation", name));
        }

        boolean active = true;
        long position = 0;

        while (active) {
            var instruction = OpcodeInstruction.of(memory.get(position), this.relativeBase);
            var opcode = instruction.getOpcode();

            if(logStages) {
                System.out.println(format(
                        "[%s] opcode %s at position %s in instruction %s",
                        name, opcode, position, Arrays.toString(instruction.getInstructions())));
            }

            switch (opcode) {
                case 1:
                    position = calculateOpcode1(position, instruction);
                    break;
                case 2:
                    position = calculateOpcode2(position, instruction);
                    break;
                case 3:
                    position = calculateOpcode3(position, instruction);
                    break;
                case 4:
                    position = calculateOpcode4(position, instruction);
                    break;
                case 5:
                    position = calculateOpcode5(position, instruction);
                    break;
                case 6:
                    position = calculateOpcode6(position, instruction);
                    break;
                case 7:
                    position = calculateOpcode7(position, instruction);
                    break;
                case 8:
                    position = calculateOpcode8(position, instruction);
                    break;
                case 9:
                    position = calculateOpcode9(position, instruction);
                    break;
                case 99:
                    active = false;
                    break;
                default:
                    throw new IllegalArgumentException(format("invalid opcode %s at position %s", opcode, position));
            }
        }

        return this.memory;
    }

    private long calculateOpcode9(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);

        this.relativeBase += firstVal.getValue();

        if(logStages) {
            System.out.println(
                    format("[%s] [%s: %s - %s] rb=%s",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            this.relativeBase
                    )
            );
        }

        return position + 2;
    }

    private long calculateOpcode8(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);
        var secondVal = instruction.getParameterValue(memory, position, 2);
        var resultVal = instruction.getParameterIndex(memory, position, 3);

        int value;
        if(firstVal.getValue() == secondVal.getValue()) {
            value = 1;
        } else {
            value = 0;
        }

        memory.set(resultVal.getValue(), value);

        if(logStages) {
            System.out.println(
                    format("[%s] [%s: %s - %s] == [%s: %s - %s] = [%s, %s = %s]",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue(),
                            resultVal.getPosition(), resultVal.getValue(), value)
            );
        }

        return position + 4;
    }

    private long calculateOpcode7(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);
        var secondVal = instruction.getParameterValue(memory, position, 2);
        var resultVal = instruction.getParameterIndex(memory, position, 3);

        int value;
        if(firstVal.getValue() < secondVal.getValue()) {
            value = 1;
        } else {
            value = 0;
        }

        memory.set(resultVal.getValue(), value);

        if(logStages) {
            System.out.println(
                    format("[%s] [%s: %s - %s] < [%s: %s - %s] = [%s, %s = %s]",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue(),
                            resultVal.getPosition(), resultVal.getValue(), value)
            );
        }

        return position + 4;
    }

    private long calculateOpcode6(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);
        var secondVal = instruction.getParameterValue(memory, position, 2);

        if(firstVal.getValue() == 0) {

            if(logStages) {
                System.out.println(
                        format("[%s] [%s, %s = %s], [%s, %s = %s]",
                                this.name,
                                firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                                secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue()
                        )
                );
            }

            return secondVal.getValue();
        }

        return position + 3;
    }

    private long calculateOpcode5(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);
        var secondVal = instruction.getParameterValue(memory, position, 2);

        if(logStages) {
            System.out.println(
                    format("[%s] [%s, %s = %s], [%s, %s = %s]",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue()
                    )
            );
        }

        if(firstVal.getValue() != 0) {
            return secondVal.getValue();
        }

        return position + 3;
    }

    private long calculateOpcode4(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);

        if(logStages) {
            System.out.println(format(
                    "[%s] opcode 4: [%s, %s = %s]",
                    this.name, firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue()));
        }

        outputDataProvider.put(firstVal.getValue());

        return position + 2;
    }

    private long calculateOpcode3(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterIndex(memory, position, 1);

        long input =  inputDataProvider.data();
        memory.set(firstVal.getValue(), input);

        if(logStages) {
            System.out.println(format(
                    "[%s] opcode 3 :[%s: %s = %s]",
                    this.name, firstVal.getPosition(), firstVal.getValueAtPosition(), input));
        }

        return position + 2;
    }

    private long calculateOpcode2(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);
        var secondVal = instruction.getParameterValue(memory, position, 2);
        var resultVal = instruction.getParameterIndex(memory, position, 3);

        long product = firstVal.getValue() * secondVal.getValue();
        memory.set(resultVal.getValue(), product);

        if(logStages) {
            System.out.println(
                    format("[%s] [%s: %s - %s] * [%s: %s - %s] = [%s, %s = %s]",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue(),
                            resultVal.getPosition(), resultVal.getValue(), product)
            );
        }

        return position + 4;
    }

    private long calculateOpcode1(long position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(memory, position, 1);
        var secondVal = instruction.getParameterValue(memory, position, 2);
        var resultVal = instruction.getParameterIndex(memory, position, 3);

        long sum = firstVal.getValue() + secondVal.getValue();
        memory.set(resultVal.getValue(), sum);

        if(logStages) {
            System.out.println(
                    format("[%s] [%s: %s - %s] + [%s: %s - %s] = [%s, %s = %s]",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue(),
                            resultVal.getPosition(), resultVal.getValue(), sum)
            );
        }

        return position + 4;
    }

    private long[] convertToInt(String[] positionsAsString) {
        return Stream
                .of(positionsAsString)
                .mapToLong(val -> Long.parseLong(val.trim()))
                .toArray();
    }

    public static void main(String[] args) {
        ListOutputDataProvider output = new ListOutputDataProvider();
        SingleInputDataProvider input = new SingleInputDataProvider(2L);

        var main = new Day9Main(
                "main",
                Path.of("day9-input.txt"),
                input,
                output
        );
        main.disableLogging();
        main.calculate();
        System.out.println("vals " + output.getData());
    }
}
