package org.krjura.adventofcode.day9;

import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.day9.providers.ListOutputDataProvider;
import org.krjura.adventofcode.day9.providers.NoInputDataProvider;

import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class Day9MainTest {

    @Test
    public void testPart1Example1() {
        ListOutputDataProvider output = new ListOutputDataProvider();
        NoInputDataProvider input = new NoInputDataProvider();

        var main = new Day9Main(
                "main",
                Path.of("day9-part1-example1.txt"),
                input,
                output
        );
        main.disableLogging();
        main.calculate();

        assertThat(output.getData())
                .isEqualTo(List.of(109L,1L,204L,-1L,1001L,100L,1L,100L,1008L,100L,16L,101L,1006L,101L,0L,99L));
    }

    @Test
    public void testPart1Example2() {
        ListOutputDataProvider output = new ListOutputDataProvider();
        NoInputDataProvider input = new NoInputDataProvider();

        var main = new Day9Main(
                "main",
                Path.of("day9-part1-example2.txt"),
                input,
                output
        );
        main.disableLogging();
        main.calculate();

        assertThat(output.getData()).hasSize(1);
        assertThat(output.getData().get(0)).isBetween(1_000_000_000_000_000L, 9_999_999_999_999_999L);
    }

    @Test
    public void testPart1Example3() {
        ListOutputDataProvider output = new ListOutputDataProvider();
        NoInputDataProvider input = new NoInputDataProvider();

        var main = new Day9Main(
                "main",
                Path.of("day9-part1-example3.txt"),
                input,
                output
        );
        main.disableLogging();
        main.calculate();

        assertThat(output.getData()).hasSize(1);
        assertThat(output.getData().get(0)).isEqualTo(1125899906842624L);
    }

}