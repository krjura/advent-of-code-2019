package org.krjura.adventofcode.day6;

import java.util.HashSet;
import java.util.function.Predicate;

public class Day6Predicates {

    private Day6Predicates() {
        // util
    }

    /**
     * Can be used in stream to filter all processed entries
     * @param processedNodes list of all processed entries
     * @return true if entry was not processed, false otherwise
     */
    public static Predicate<String> processed(HashSet<String> processedNodes) {
        return object -> ! processedNodes.contains(object);
    }
}
