package org.krjura.adventofcode.day6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListUtils {

    private ListUtils() {
        // util
    }

    public static List<String> merge(List<String> o1, List<String> o2) {
        var merge = new ArrayList<String>(o1.size() + o2.size());
        merge.addAll(o1);
        merge.addAll(o2);

        return Collections.unmodifiableList(merge);
    }

    public static List<String> addCurrentToStops(FlightInfo info) {
        var merge = new ArrayList<String>(info.getStops().size() + 1);
        merge.addAll(info.getStops());
        merge.add(info.getCurrent());

        return Collections.unmodifiableList(merge);
    }
}
