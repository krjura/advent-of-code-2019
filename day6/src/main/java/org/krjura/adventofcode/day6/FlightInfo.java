package org.krjura.adventofcode.day6;

import java.util.List;
import java.util.Objects;

public class FlightInfo {

    private String current;
    private List<String> stops;

    private FlightInfo(String current, List<String> stops) {
        this.current = Objects.requireNonNull(current);
        this.stops = Objects.requireNonNull(stops);
    }

    public static FlightInfo of(String current, List<String> stops) {
        return new FlightInfo(current, stops);
    }

    public String getCurrent() {
        return current;
    }

    public List<String> getStops() {
        return stops;
    }

    @Override
    public String toString() {
        return "FlightInfo{" +
                "current='" + current + '\'' +
                ", stops=" + stops +
                '}';
    }
}
