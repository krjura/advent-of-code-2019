package org.krjura.adventofcode.day6;

import java.util.function.Function;

public class ObjectOrbitParser {

    private ObjectOrbitParser() {
        // util
    }

    public static Function<String, ObjectOrbit> toOrbitInfo() {
        return ObjectOrbitParser::toOrbitInfo;
    }

    private static ObjectOrbit toOrbitInfo(String data) {
        String[] params = data.split("\\)");

        if(params.length != 2) {
            throw new RuntimeException("invalid orbit info " + data);
        }

        return ObjectOrbit.of(params[1].strip(), params[0].strip());
    }
}
