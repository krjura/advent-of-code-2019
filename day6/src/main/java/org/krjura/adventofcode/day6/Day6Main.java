package org.krjura.adventofcode.day6;

import static java.lang.String.format;
import static org.krjura.adventofcode.day6.Day6MainUtils.calculateChildren;
import static org.krjura.adventofcode.day6.Day6MainUtils.checkOrbitInfo;
import static org.krjura.adventofcode.day6.Day6MainUtils.isCurrentCom;
import static org.krjura.adventofcode.day6.Day6MainUtils.isCurrentSan;
import static org.krjura.adventofcode.day6.Day6MainUtils.isParentCom;
import static org.krjura.adventofcode.day6.Day6MainUtils.parseOrbitInput;
import static org.krjura.adventofcode.day6.Day6Predicates.processed;
import static org.krjura.adventofcode.day6.ListUtils.addCurrentToStops;

import java.nio.file.Path;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Day6Main {

    private Map<String, ObjectOrbit> objectParents;
    private Map<String, List<String>> objectChildren;

    public Day6Main(Path orbitInput) {
        this.objectParents = parseOrbitInput(orbitInput);
        this.objectChildren = calculateChildren(this.objectParents);
    }

    public Map<String, ObjectOrbit> getObjectParents() {
        return objectParents;
    }

    public int calculateFlight() {

        var solutions = new LinkedList<FlightInfo>();
        var taskQueue = initializeTaskQueue();
        var processedObjects = new HashSet<String>();

        while (taskQueue.size() != 0) {
            var flightInfo = taskQueue.remove();
            processedObjects.add(flightInfo.getCurrent());

            if(isCurrentSan(flightInfo)) {
                solutions.add(flightInfo);
                continue;
            } else if(isCurrentCom(flightInfo)) {
                // end of line. just ignore
                continue;
            }

            var orbitInfo = this.objectParents.get(flightInfo.getCurrent());

            // // add all children
            findUnprocessedChildren(processedObjects, flightInfo)
                    .forEach(addToQueue(taskQueue, flightInfo));

            // add parent if not processed
            if(! processedObjects.contains(orbitInfo.getParent())) {
                taskQueue.add(generateFlightInfo(orbitInfo.getParent(), flightInfo));
            }
        }

        System.out.println(format("found %s solutions: %s", solutions.size(), solutions));

        var result = solutions
                .stream()
                .mapToInt(value -> value.getStops().size() - 2) //-2 YOU, parent which are included in stops
                .min();

        return result.orElseThrow(() -> new RuntimeException("so solution found"));
    }

    private Deque<FlightInfo> initializeTaskQueue() {
        Deque<FlightInfo> possibleSolutions = new LinkedList<>();
        possibleSolutions.add(FlightInfo.of("YOU", List.of()));
        return possibleSolutions;
    }

    private FlightInfo generateFlightInfo(String parent, FlightInfo info) {
        return FlightInfo.of(parent, addCurrentToStops(info));
    }

    private Consumer<String> addToQueue(Deque<FlightInfo> queue, FlightInfo flightInfo) {
        return child -> queue.addFirst(generateFlightInfo(child, flightInfo));
    }

    private List<String> findUnprocessedChildren(HashSet<String> processedNodes, FlightInfo flightInfo) {
        return this
                .objectChildren
                .getOrDefault(flightInfo.getCurrent(), List.of())
                .stream()
                .filter(processed(processedNodes))
                .collect(Collectors.toList());
    }

    public long calculateCheckSum() {
        return this.objectParents
                .keySet()
                .stream()
                .map(this::calculateNumberOfOrbits)
                .mapToInt(val -> val)
                .sum();
    }

    private Integer calculateNumberOfOrbits(String startObject) {

        String currentObject = startObject;
        int orbitCount = 0;

        do {
            var orbitInfo = this.objectParents.get(currentObject);
            checkOrbitInfo(currentObject, orbitInfo);

            if(isParentCom(orbitInfo)) {
                currentObject = null;
            } else {
                currentObject = orbitInfo.getParent();
            }

            orbitCount++;

        } while (currentObject != null);

        System.out.println(format("object %s has %s direct and indirect orbits", startObject, orbitCount));

        return orbitCount;
    }

    public static void main(String[] args) {

        double attempts = 0;
        double sum = 0;

        for(int i = 0; i< 200; i++) {
            var start = System.currentTimeMillis();

            var main = new Day6Main(Path.of("day6-input.txt"));
            var min = main.calculateFlight();

            var end = System.currentTimeMillis();

            sum += (double) ( end - start );
            attempts += 1.0d;

            System.out.println(format("it took %s", (end - start)));
            System.out.println(format("orbits = %s", min));
        }

        System.out.println(String.format("it took %s ms in avg", ( sum / attempts )));
    }
}
