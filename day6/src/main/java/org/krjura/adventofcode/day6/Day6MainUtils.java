package org.krjura.adventofcode.day6;

import static java.util.stream.Collectors.toMap;
import static org.krjura.adventofcode.day6.ObjectOrbitParser.toOrbitInfo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class Day6MainUtils {

    private Day6MainUtils() {
        // util
    }

    public static  Map<String, List<String>> calculateChildren(Map<String, ObjectOrbit> orbitMap) {
        return orbitMap
                .values()
                .stream()
                .collect(toMap(ObjectOrbit::getParent, oi -> List.of(oi.getCurrent()), ListUtils::merge));
    }

    public static Map<String, ObjectOrbit> parseOrbitInput(Path orbitInput) {
        return readLines(orbitInput)
                .stream()
                .map(toOrbitInfo())
                .collect(toMap(ObjectOrbit::getCurrent, Function.identity()));
    }

    private static List<String> readLines(Path orbitInput) {
        try {
            return Files.readAllLines(orbitInput, StandardCharsets.UTF_8);
        } catch(IOException e) {
            throw new RuntimeException("cannot read lines from path: " + orbitInput, e);
        }
    }

    public static boolean isParentCom(ObjectOrbit orbitInfo) {
        return orbitInfo.getParent().equals("COM");
    }

    public static boolean isCurrentSan(FlightInfo info) {
        return info.getCurrent().equals("SAN");
    }

    public static boolean isCurrentCom(FlightInfo info) {
        return info.getCurrent().equals("COM");
    }

    public static void checkOrbitInfo(String current, ObjectOrbit orbitInfo) {
        if(orbitInfo == null) {
            throw new IllegalArgumentException("object " + current + " does not have parent");
        }
    }
}
