package org.krjura.adventofcode.day6;

import java.util.Objects;

public class ObjectOrbit {

    private final String current;
    private final String parent;

    private ObjectOrbit(String current, String parent) {
        this.current = Objects.requireNonNull(current);
        this.parent = Objects.requireNonNull(parent);
    }

    public static ObjectOrbit of(String current, String parent) {
        return new ObjectOrbit(current, parent);
    }

    public String getCurrent() {
        return current;
    }

    public String getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "OrbitInfo{" +
                "current='" + current + '\'' +
                ", parent='" + parent + '\'' +
                '}';
    }
}
