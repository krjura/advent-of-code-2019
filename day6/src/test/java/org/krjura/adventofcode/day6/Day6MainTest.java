package org.krjura.adventofcode.day6;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import org.junit.jupiter.api.Test;

public class Day6MainTest {

    @Test
    public void part1Example() {
        var main = new Day6Main(Path.of("day6-part1-example.txt"));

        assertThat(main.calculateCheckSum()).isEqualTo(42);
    }

    @Test
    public void part2Example() {
        var main = new Day6Main(Path.of("day6-part2-example.txt"));

        assertThat(main.calculateFlight()).isEqualTo(4);
    }
}