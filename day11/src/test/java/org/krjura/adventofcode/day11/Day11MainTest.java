package org.krjura.adventofcode.day11;

import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.common.pojos.Coordinate;
import org.krjura.adventofcode.day11.enums.Direction;
import org.krjura.adventofcode.day11.enums.Paint;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class Day11MainTest {

    @Test
    public void testExample() {
        var main = new Day11Main(Path.of("day11-input.txt"), Coordinate.of(2, 2), Paint.BLACK);

        var result = main.moveOnce();

        assertThat(result).isNotNull();
        assertThat(result.isDone()).isFalse();
        assertThat(result.getRobot().getPosition()).isEqualTo(Coordinate.of(1, 2));
        assertThat(result.getRobot().getDirection()).isEqualTo(Direction.LEFT);
        assertThat(result.getPaint()).isEqualTo(Paint.WHITE);
    }

    @Test
    public void testPart1Complete() {
        var main = new Day11Main(Path.of("day11-input.txt"), Coordinate.of(2, 2), Paint.BLACK);
        var count = main.moveUntilDone();

        assertThat(count).isEqualTo(2469);
    }

    @Test
    public void testPart2Complete() {
        var main = new Day11Main(Path.of("day11-input.txt"), Coordinate.of(0, 0), Paint.WHITE);
        var count = main.moveUntilDone();

        assertThat(count).isEqualTo(249L);
    }
}