package org.krjura.adventofcode.day11.pojo;

import org.krjura.adventofcode.common.pojos.Coordinate;
import org.krjura.adventofcode.day11.enums.Direction;

import java.util.Objects;

public class Robot {

    private final Coordinate position;
    private final Direction direction;

    public Robot(Coordinate position, Direction direction) {
        this.position = Objects.requireNonNull(position);
        this.direction = Objects.requireNonNull(direction);
    }

    public static Robot of(Coordinate robotPosition, Direction robotDirection) {
        return new Robot(robotPosition, robotDirection);
    }

    public Coordinate getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    public Robot moveUp() {
        return new Robot(getPosition().incrementY(), Direction.UP);
    }

    public Robot moveDown() {
        return new Robot(getPosition().decrementY(), Direction.DOWN);
    }

    public Robot moveLeft() {
        return new Robot(getPosition().decrementX(), Direction.LEFT);
    }

    public Robot moveRight() {
        return new Robot(getPosition().incrementX(), Direction.RIGHT);
    }
}
