package org.krjura.adventofcode.day11.enums;

public enum Direction {

    UP,
    DOWN,
    LEFT,
    RIGHT
}
