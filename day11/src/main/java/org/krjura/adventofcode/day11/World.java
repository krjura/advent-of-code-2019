package org.krjura.adventofcode.day11;

import org.krjura.adventofcode.common.pojos.Coordinate;
import org.krjura.adventofcode.common.pojos.CoordinateLimits;
import org.krjura.adventofcode.day11.enums.Paint;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class World {

    private Map<Coordinate, Paint> data;

    public World() {
        this.data = new HashMap<>();
    }

    public void set(Coordinate coordinate, Paint paint) {
        this.data.put(coordinate, paint);
    }

    public Paint get(Coordinate coordinate) {
        return this.data.getOrDefault(coordinate, Paint.DEFAULT);
    }

    public Map<Coordinate, Paint> getData() {
        return Collections.unmodifiableMap(data);
    }

    public void print() {
        var limits = CoordinateLimits.of(this.data.keySet());

        System.out.println("");
        for(int j = limits.maxY; j >= limits.minY; j--) {
            System.out.println("");

            for(int i = limits.minX; i <= limits.maxX; i++) {
                var paint = this.data.getOrDefault(Coordinate.of(i, j), Paint.BLACK);

                switch (paint) {
                    case BLACK:
                        System.out.print(" ");
                        break;
                    case WHITE:
                        System.out.print("X");
                        break;
                }
            }
        }
    }
}
