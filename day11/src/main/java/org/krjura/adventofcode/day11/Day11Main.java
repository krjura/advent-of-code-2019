package org.krjura.adventofcode.day11;

import org.krjura.adventofcode.common.intcode.AsyncIntCodeComputer;
import org.krjura.adventofcode.common.intcode.OutputData;
import org.krjura.adventofcode.common.intcode.providers.impl.QueueInputDataProvider;
import org.krjura.adventofcode.common.intcode.providers.impl.QueueOutputDataProvider;
import org.krjura.adventofcode.common.pojos.Coordinate;
import org.krjura.adventofcode.day11.enums.Direction;
import org.krjura.adventofcode.day11.enums.Paint;
import org.krjura.adventofcode.day11.enums.Turn;
import org.krjura.adventofcode.day11.pojo.MoveResult;
import org.krjura.adventofcode.day11.pojo.Robot;

import java.nio.file.Path;

import static java.lang.String.format;

public class Day11Main {

    private final QueueInputDataProvider input;
    private final QueueOutputDataProvider output;
    private final AsyncIntCodeComputer computer;

    private final World world;
    private Robot robot;

    public Day11Main(Path inputPath, Coordinate startPoint, Paint startingColor) {
        this.input = QueueInputDataProvider.of();
        this.output = QueueOutputDataProvider.of();
        this.computer = new AsyncIntCodeComputer("cpu0", inputPath, this.input, this.output);
        this.computer.start();

        this.robot = Robot.of(startPoint, Direction.UP);
        this.world = new World();
        this.world.set(startPoint, startingColor);
    }

    public long moveUntilDone() {

        var result = MoveResult.of(false, this.robot, Paint.BLACK);
        while (! result.isDone()) {
            result = moveOnce();
        }

        return this
                .world
                .getData()
                .values()
                .stream()
                .filter(paint -> paint != Paint.DEFAULT ) // remove default
                .count();
    }

    public MoveResult moveOnce() {
        long color = calculateColorAtPosition();

        this.input.put(color);

        OutputData colorData = this.output.get();

        // it is possible that isComputerDone this not complete in time since it is running in parallel
        // to we need to check which opcode was executed last
        // 99 is exit
        if(colorData.getOpcode() == 99) {
            return MoveResult.of(true, this.robot, Paint.fromCode(color));
        }

        OutputData turnData = this.output.get();

        if(turnData.getOpcode() == 99) {
            return MoveResult.of(true, this.robot, Paint.fromCode(color));
        }

        return colorAndTurn(colorData.getData(), turnData.getData());
    }

    private MoveResult colorAndTurn(long colorCode, long turnCode) {
        var paint = Paint.fromCode(colorCode);
        var turn = Turn.fromCode(turnCode);

        this.world.set(this.robot.getPosition(), paint);

        if(this.robot.getDirection() == Direction.UP && turn == Turn.LEFT) {
            this.robot = this.robot.moveLeft();
        } else if(this.robot.getDirection() == Direction.UP && turn == Turn.RIGHT) {
            this.robot = this.robot.moveRight();
        } else if(this.robot.getDirection() == Direction.DOWN && turn == Turn.LEFT) {
            this.robot = this.robot.moveRight();
        } else if(this.robot.getDirection() == Direction.DOWN && turn == Turn.RIGHT) {
            this.robot = this.robot.moveLeft();
        } else if(this.robot.getDirection() == Direction.LEFT && turn == Turn.LEFT) {
            this.robot = this.robot.moveDown();
        } else if(this.robot.getDirection() == Direction.LEFT && turn == Turn.RIGHT) {
            this.robot = this.robot.moveUp();
        } else if(this.robot.getDirection() == Direction.RIGHT && turn == Turn.LEFT) {
            this.robot = this.robot.moveUp();
        } else if(this.robot.getDirection() == Direction.RIGHT && turn == Turn.RIGHT) {
            this.robot = this.robot.moveDown();
        } else {
            throw new RuntimeException(format("invalid turn %s for position %s", turn, this.robot.getPosition()));
        }

        return MoveResult.of(false, this.robot, paint);
    }

    private long calculateColorAtPosition() {
        var positionColor = this.world.get(this.robot.getPosition());

        if(positionColor == Paint.DEFAULT) {
            return Paint.BLACK.getCode();
        } else if(positionColor == Paint.BLACK) {
            return Paint.BLACK.getCode();
        } else {
            return Paint.WHITE.getCode();
        }
    }

    public static void main(String[] args) {
        var main = new Day11Main(Path.of("day11-input.txt"), Coordinate.of(0, 0), Paint.WHITE);
        var count = main.moveUntilDone();

        System.out.println("done");
        main.world.print();
        // KLCZAEGU
    }
}
