package org.krjura.adventofcode.day11.enums;

public enum Turn {

    LEFT(0),
    RIGHT(1)
    ;

    private final long code;

    Turn(long code) {
        this.code = code;
    }

    public long getCode() {
        return code;
    }

    public static Turn fromCode(long code) {
        if(code == 0) {
            return LEFT;
        } else if(code == 1) {
            return RIGHT;
        } else {
            throw new RuntimeException("invalid turn code of " + code);
        }
    }
}
