package org.krjura.adventofcode.day11.enums;

public enum  Paint {

    BLACK(0),
    WHITE(1),
    DEFAULT(-1)
    ;

    private long code;

    Paint(int code) {
        this.code = code;
    }

    public long getCode() {
        return code;
    }

    public static Paint fromCode(long code) {
        if( code == 0) {
            return BLACK;
        } else if(code == 1) {
            return WHITE;
        } else {
            throw new RuntimeException("unknown color code of " + code);
        }
    }
}
