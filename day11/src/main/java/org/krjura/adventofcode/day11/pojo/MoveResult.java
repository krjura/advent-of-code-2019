package org.krjura.adventofcode.day11.pojo;

import org.krjura.adventofcode.day11.enums.Paint;

import java.util.Objects;

public class MoveResult {

    private final boolean done;

    private final Robot robot;

    private final Paint paint;

    public MoveResult(boolean done, Robot robot, Paint paint) {
        this.done = done;
        this.robot = Objects.requireNonNull(robot);
        this.paint = Objects.requireNonNull(paint);
    }

    public static MoveResult of(boolean done, Robot robot, Paint paint) {
        return new MoveResult(done, robot, paint);
    }

    public boolean isDone() {
        return done;
    }

    public Robot getRobot() {
        return robot;
    }

    public Paint getPaint() {
        return paint;
    }
}