package org.krjura.adventofcode.day12;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.krjura.adventofcode.day12.pojos.Moon;
import org.krjura.adventofcode.day12.pojos.Position;
import org.krjura.adventofcode.day12.pojos.Velocity;

import static org.krjura.adventofcode.day12.utils.FileUtils.parseInput;

public class Day12Main {

    private List<Moon> moons;

    public Day12Main(Path inputPath) {
        this.moons = parseInput(inputPath);
    }

    public void moveOnce() {
        this.moons = moons
                .stream()
                .map(moon -> adjustPositionAndVelocity(moon, this.moons))
                .collect(Collectors.toList());
    }

    public void moveN(int count) {
        for(int i = 0; i < count; i++) {
            moveOnce();
        }
    }

    private Moon adjustPositionAndVelocity(Moon current, List<Moon> moons) {

        // could be done with reduce but performance is at leat 50% worst
        var newV = current.velocity();

        for(Moon moon : moons) {
            if(moon == current) {
                continue; // ignore self
            }

            newV = updateVelocity(current, moon , newV);
        }

        return new Moon(current.name(), current.position().adjust(newV), newV);
    }

    public Velocity updateVelocity(Moon current, Moon next, Velocity currentVelocity) {
        int x = 0;
        int y = 0;
        int z = 0;

        if(current.position().x() < next.position().x()) {
            x += 1;
        } else if(current.position().x() > next.position().x()) {
            x += -1;
        }

        if(current.position().y() < next.position().y()) {
            y += 1;
        } else if(current.position().y() > next.position().y()) {
            y += -1;
        }

        if(current.position().z() < next.position().z()) {
            z += 1;
        } else if(current.position().z() > next.position().z()) {
            z += -1;
        }

        return currentVelocity.append(x, y, z);
    }

    public List<Moon> getMoons() {
        return Collections.unmodifiableList(this.moons);
    }
}
