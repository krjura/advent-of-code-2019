package org.krjura.adventofcode.day12.utils;

import org.krjura.adventofcode.day12.pojos.Moon;
import org.krjura.adventofcode.day12.pojos.Position;
import org.krjura.adventofcode.day12.pojos.Velocity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;

public class FileUtils {

    private static final Pattern PATTERN_LOCATION = Pattern.compile("<x=(?<x>-?\\d+), y=(?<y>-?\\d+), z=(?<z>-?\\d+)>");

    private FileUtils() {
        // utils
    }

    public static List<Moon> parseInput(Path inputPath) {
        try {
            return Files
                    .lines(inputPath)
                    .map(FileUtils::parseLine)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException("Cannot read input path " + inputPath, e);
        }
    }

    private static Moon parseLine(String line) {
        var matcher = PATTERN_LOCATION.matcher(line);
        if(!matcher.find()) {
            throw new RuntimeException("invalid input line " + line);
        }

        int x = parseInt(matcher.group("x"));
        int y = parseInt(matcher.group("y"));
        int z = parseInt(matcher.group("z"));

        return new Moon(line, Position.of(x, y, z), Velocity.of(0, 0, 0));
    }
}
