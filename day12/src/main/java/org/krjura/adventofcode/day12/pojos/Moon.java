package org.krjura.adventofcode.day12.pojos;

import java.util.Objects;

public class Moon {

    private final String name;

    private final Position position;

    private final Velocity velocity;

    public Moon(String name, Position position, Velocity velocity) {
        this.name = Objects.requireNonNull(name);
        this.position = Objects.requireNonNull(position);
        this.velocity = Objects.requireNonNull(velocity);
    }

    public String name() {
        return name;
    }

    public Position position() {
        return position;
    }

    public Velocity velocity() {
        return velocity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Moon moon = (Moon) o;
        return name.equals(moon.name) &&
                position.equals(moon.position) &&
                velocity.equals(moon.velocity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, position, velocity);
    }

    @Override
    public String toString() {
        return "Moon{" +
                "name='" + name + '\'' +
                ", position=" + position +
                ", velocity=" + velocity +
                '}';
    }
}
