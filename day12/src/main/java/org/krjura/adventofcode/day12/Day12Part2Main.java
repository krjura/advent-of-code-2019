package org.krjura.adventofcode.day12;

import org.krjura.adventofcode.day12.pojos.Moon;

import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.String.format;
import static org.krjura.adventofcode.day12.utils.Baeldung.lcm;

public class Day12Part2Main {

    public static long toInitial(Path inputPath) {
        var main = new Day12Main(inputPath);

        var checks = List.of(
            OrbitCheck.of(main.getMoons(), Day12Part2Main::xOrbitCheck),
            OrbitCheck.of(main.getMoons(), Day12Part2Main::yOrbitCheck),
            OrbitCheck.of(main.getMoons(), Day12Part2Main::zOrbitCheck)
        );

        AtomicLong count = new AtomicLong(0);
        while (! allChecksDone(checks)) {
            main.moveOnce();
            count.incrementAndGet();

            checks.forEach(it -> it.match(main.getMoons(), count.get()));
        }

        System.out.println(format("stopped at %s", count));

        return lcm(checks);
    }

    private static boolean allChecksDone(List<OrbitCheck> checks) {
        return checks
                .stream()
                .allMatch(OrbitCheck::isDone);
    }

    private static boolean zOrbitCheck(Moon moon1, Moon moon2) {
        return moon1.position().z() == moon2.position().z() && moon1.velocity().z() == moon2.velocity().z();
    }

    private static boolean yOrbitCheck(Moon moon1, Moon moon2) {
        return moon1.position().y() == moon2.position().y() && moon1.velocity().y() == moon2.velocity().y();
    }

    private static boolean xOrbitCheck(Moon moon1, Moon moon2) {
        return moon1.position().x() == moon2.position().x() && moon1.velocity().x() == moon2.velocity().x();
    }
}
