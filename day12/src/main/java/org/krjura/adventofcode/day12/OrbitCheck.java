package org.krjura.adventofcode.day12;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import org.krjura.adventofcode.day12.pojos.Moon;

import static java.lang.String.format;

public class OrbitCheck {

    private final List<Moon> initial;
    private final BiFunction<Moon, Moon, Boolean> matcher;

    private Long iterations;

    public OrbitCheck(List<Moon> initial, BiFunction<Moon, Moon, Boolean> matcher) {
        this.initial = Objects.requireNonNull(initial);
        this.matcher = matcher;
    }

    public static OrbitCheck of(List<Moon> initial, BiFunction<Moon, Moon, Boolean> matcher) {
        return new OrbitCheck(initial, matcher);
    }

    public Long getIterations() {
        return iterations;
    }

    public void match(List<Moon> current, long iteration) {
        verifySets(current);

        if(iterations != null) {
            return;
        }

        for(int i = 0, n = current.size(); i < n; i++) {
            // quit if any do not match
            var moon1 = initial.get(i);
            var moon2 = current.get(i);
            checkMoons(moon1, moon2);

            if(!matcher.apply(moon1, moon2)) {
               return;
            }
        }

        iterations = iteration;
    }

    private void checkMoons(Moon moon1, Moon moon2) {
        if(!moon1.name().equals(moon2.name())) {
            throw new RuntimeException(format("comparing different moons %s != %s", moon1, moon2));
        }
    }

    private void verifySets(List<Moon> current) {
        if(current.size() != initial.size()) {
            throw new RuntimeException(format("sets are not the same %s != %s", current, initial));
        }
    }

    public boolean isDone() {
        return iterations != null;
    }
}
