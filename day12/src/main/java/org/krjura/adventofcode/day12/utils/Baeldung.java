package org.krjura.adventofcode.day12.utils;

import org.krjura.adventofcode.day12.OrbitCheck;

import java.util.List;

public class Baeldung {

    public static long lcm(List<OrbitCheck> checks) {
        return checks
                .stream()
                .map(OrbitCheck::getIterations)
                .reduce(1L, Baeldung::lcm);
    }

    // https://www.baeldung.com/java-least-common-multiple
    public static long lcm(long number1, long number2) {
        if (number1 == 0 || number2 == 0) {
            return 0;
        }

        long absNumber1 = Math.abs(number1);
        long absNumber2 = Math.abs(number2);
        long absHigherNumber = Math.max(absNumber1, absNumber2);
        long absLowerNumber = Math.min(absNumber1, absNumber2);
        long lcm = absHigherNumber;

        while (lcm % absLowerNumber != 0) {
            lcm += absHigherNumber;
        }

        return lcm;
    }
}
