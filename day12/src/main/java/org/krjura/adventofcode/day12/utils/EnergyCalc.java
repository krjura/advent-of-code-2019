package org.krjura.adventofcode.day12.utils;

import org.krjura.adventofcode.day12.pojos.Moon;

import static java.lang.Math.abs;

public class EnergyCalc {

    private EnergyCalc() {
        // util
    }

    public static int potentialEnergy(Moon moon) {
        return abs(moon.position().x()) + abs(moon.position().y()) + abs(moon.position().z());
    }

    public static int kineticEnergy(Moon moon) {
        return abs(moon.velocity().x()) + abs(moon.velocity().y()) + abs(moon.velocity().z());
    }

    public static int totalEnergy(Moon moon) {
        return potentialEnergy(moon) * kineticEnergy(moon);
    }
}
