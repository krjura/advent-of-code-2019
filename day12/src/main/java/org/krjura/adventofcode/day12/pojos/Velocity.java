package org.krjura.adventofcode.day12.pojos;

import java.util.Objects;

public class Velocity {

    private final int x;
    private final int y;
    private final int z;

    public Velocity(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static Velocity of(int x, int y, int z) {
        return new Velocity(x, y, z);
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public int z() {
        return z;
    }

    public Velocity append(int x2, int y2, int z2) {
        return Velocity.of(x + x2, y + y2, z + z2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Velocity velocity = (Velocity) o;
        return x == velocity.x &&
                y == velocity.y &&
                z == velocity.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    @Override
    public String toString() {
        return "Velocity{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}