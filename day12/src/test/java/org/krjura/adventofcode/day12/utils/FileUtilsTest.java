package org.krjura.adventofcode.day12.utils;

import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.day12.pojos.Position;
import org.krjura.adventofcode.day12.pojos.Velocity;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class FileUtilsTest {

    @Test
    public void testInput() {
        var moons = FileUtils.parseInput(Path.of("day12-input.txt"));

        assertThat(moons).hasSize(4);

        assertThat(moons.get(0).name()).isEqualTo("<x=-19, y=-4, z=2>");
        assertThat(moons.get(0).position()).isEqualTo(Position.of(-19, -4, 2));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(0, 0, 0));

        assertThat(moons.get(1).name()).isEqualTo("<x=-9, y=8, z=-16>");
        assertThat(moons.get(1).position()).isEqualTo(Position.of(-9, 8, -16));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(0, 0, 0));

        assertThat(moons.get(2).name()).isEqualTo("<x=-4, y=5, z=-11>");
        assertThat(moons.get(2).position()).isEqualTo(Position.of(-4, 5, -11));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(0, 0, 0));

        assertThat(moons.get(3).name()).isEqualTo("<x=1, y=9, z=-13>");
        assertThat(moons.get(3).position()).isEqualTo(Position.of(1, 9, -13));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(0, 0, 0));
    }
}