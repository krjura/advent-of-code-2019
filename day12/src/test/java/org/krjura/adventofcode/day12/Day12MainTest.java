package org.krjura.adventofcode.day12;

import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.day12.pojos.Position;
import org.krjura.adventofcode.day12.pojos.Velocity;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.adventofcode.day12.utils.EnergyCalc.kineticEnergy;
import static org.krjura.adventofcode.day12.utils.EnergyCalc.potentialEnergy;
import static org.krjura.adventofcode.day12.utils.EnergyCalc.totalEnergy;

public class Day12MainTest {

    @Test
    public void testExample1() {
        var main = new Day12Main(Path.of("day12-example1.txt"));

        // step 1
        main.moveOnce();
        var moons = main.getMoons();

        assertThat(moons).hasSize(4);
        assertThat(moons.get(0).position()).isEqualTo(Position.of(2, -1, 1));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(3, -1, -1));
        assertThat(moons.get(1).position()).isEqualTo(Position.of(3, -7, -4));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(1, 3, 3));
        assertThat(moons.get(2).position()).isEqualTo(Position.of(1, -7, 5));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(-3, 1, -3));
        assertThat(moons.get(3).position()).isEqualTo(Position.of(2, 2, 0));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(-1, -3, 1));

        // step 2
        main.moveOnce();
        moons = main.getMoons();

        assertThat(moons).hasSize(4);
        assertThat(moons.get(0).position()).isEqualTo(Position.of(5, -3, -1));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(3, -2, -2));
        assertThat(moons.get(1).position()).isEqualTo(Position.of(1, -2, 2));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(-2, 5, 6));
        assertThat(moons.get(2).position()).isEqualTo(Position.of(1, -4, -1));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(0, 3, -6));
        assertThat(moons.get(3).position()).isEqualTo(Position.of(1, -4, 2));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(-1, -6, 2));

        // step 3
        main.moveOnce();
        moons = main.getMoons();

        assertThat(moons).hasSize(4);
        assertThat(moons.get(0).position()).isEqualTo(Position.of(5, -6, -1));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(0, -3, 0));
        assertThat(moons.get(1).position()).isEqualTo(Position.of(0, 0, 6));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(-1, 2, 4));
        assertThat(moons.get(2).position()).isEqualTo(Position.of(2, 1, -5));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(1, 5, -4));
        assertThat(moons.get(3).position()).isEqualTo(Position.of(1, -8, 2));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(0, -4, 0));

        // step 4
        main.moveOnce();
        moons = main.getMoons();

        assertThat(moons).hasSize(4);
        assertThat(moons.get(0).position()).isEqualTo(Position.of(2, -8, 0));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(-3, -2, 1));
        assertThat(moons.get(1).position()).isEqualTo(Position.of(2, 1, 7));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(2, 1, 1));
        assertThat(moons.get(2).position()).isEqualTo(Position.of(2, 3, -6));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(0, 2, -1));
        assertThat(moons.get(3).position()).isEqualTo(Position.of(2, -9, 1));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(1, -1, -1));

        // step 5
        main.moveOnce();
        // step 6
        main.moveOnce();
        // step 7
        main.moveOnce();
        // step 8
        main.moveOnce();
        // step 9
        main.moveOnce();

        // step 10
        main.moveOnce();
        moons = main.getMoons();

        assertThat(moons).hasSize(4);
        assertThat(moons.get(0).position()).isEqualTo(Position.of(2, 1, -3));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(-3, -2, 1));
        assertThat(moons.get(1).position()).isEqualTo(Position.of(1, -8, 0));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(-1, 1, 3));
        assertThat(moons.get(2).position()).isEqualTo(Position.of(3, -6, 1));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(3, 2, -3));
        assertThat(moons.get(3).position()).isEqualTo(Position.of(2, 0, 4));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(1, -1, -1));

        assertThat(potentialEnergy(moons.get(0))).isEqualTo(6);
        assertThat(kineticEnergy(moons.get(0))).isEqualTo(6);
        assertThat(totalEnergy(moons.get(0))).isEqualTo(36);

        assertThat(potentialEnergy(moons.get(1))).isEqualTo(9);
        assertThat(kineticEnergy(moons.get(1))).isEqualTo(5);
        assertThat(totalEnergy(moons.get(1))).isEqualTo(45);

        assertThat(potentialEnergy(moons.get(2))).isEqualTo(10);
        assertThat(kineticEnergy(moons.get(2))).isEqualTo(8);
        assertThat(totalEnergy(moons.get(2))).isEqualTo(80);

        assertThat(potentialEnergy(moons.get(3))).isEqualTo(6);
        assertThat(kineticEnergy(moons.get(3))).isEqualTo(3);
        assertThat(totalEnergy(moons.get(3))).isEqualTo(18);
    }

    @Test
    public void testExample2() {
        var main = new Day12Main(Path.of("day12-example2.txt"));

        // step 10
        main.moveN(10);
        var moons = main.getMoons();

        assertThat(moons).hasSize(4);
        assertThat(moons.get(0).position()).isEqualTo(Position.of(-9, -10, 1));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(-2, -2, -1));
        assertThat(moons.get(1).position()).isEqualTo(Position.of(4, 10, 9));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(-3, 7, -2));
        assertThat(moons.get(2).position()).isEqualTo(Position.of(8, -10, -3));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(5, -1, -2));
        assertThat(moons.get(3).position()).isEqualTo(Position.of(5, -10, 3));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(0, -4, 5));

        // step 100
        main.moveN(90); // 90 + 10 = 100
        moons = main.getMoons();

        assertThat(moons).hasSize(4);
        assertThat(moons.get(0).position()).isEqualTo(Position.of(8, -12, -9));
        assertThat(moons.get(0).velocity()).isEqualTo(Velocity.of(-7, 3, 0));
        assertThat(moons.get(1).position()).isEqualTo(Position.of(13, 16, -3));
        assertThat(moons.get(1).velocity()).isEqualTo(Velocity.of(3, -11, -5));
        assertThat(moons.get(2).position()).isEqualTo(Position.of(-29, -11, -1));
        assertThat(moons.get(2).velocity()).isEqualTo(Velocity.of(-3, 7, 4));
        assertThat(moons.get(3).position()).isEqualTo(Position.of(16, -13, 23));
        assertThat(moons.get(3).velocity()).isEqualTo(Velocity.of(7, 1, 1));

        assertThat(potentialEnergy(moons.get(0))).isEqualTo(29);
        assertThat(kineticEnergy(moons.get(0))).isEqualTo(10);
        assertThat(totalEnergy(moons.get(0))).isEqualTo(290);

        assertThat(potentialEnergy(moons.get(1))).isEqualTo(32);
        assertThat(kineticEnergy(moons.get(1))).isEqualTo(19);
        assertThat(totalEnergy(moons.get(1))).isEqualTo(608);

        assertThat(potentialEnergy(moons.get(2))).isEqualTo(41);
        assertThat(kineticEnergy(moons.get(2))).isEqualTo(14);
        assertThat(totalEnergy(moons.get(2))).isEqualTo(574);

        assertThat(potentialEnergy(moons.get(3))).isEqualTo(52);
        assertThat(kineticEnergy(moons.get(3))).isEqualTo(9);
        assertThat(totalEnergy(moons.get(3))).isEqualTo(468);
    }
}