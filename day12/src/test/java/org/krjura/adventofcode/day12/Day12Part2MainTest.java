package org.krjura.adventofcode.day12;

import java.nio.file.Path;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Day12Part2MainTest {

    @Test
    public void testExample1() {
        var start = System.currentTimeMillis();

        var count = Day12Part2Main.toInitial(Path.of("day12-example1.txt"));

        assertThat(count).isEqualTo(2772);

        var end = System.currentTimeMillis();
        System.out.println(String.format("it took %s ms", ( end - start)));
    }

    @Test
    public void testExample2() {
        var start = System.currentTimeMillis();

        var count = Day12Part2Main.toInitial(Path.of("day12-example2.txt"));

        assertThat(count).isEqualTo(4686774924L);

        var end = System.currentTimeMillis();
        System.out.println(String.format("it took %s ms", ( end - start)));
    }

    @Test
    public void testInput() {
        var start = System.currentTimeMillis();

        var count = Day12Part2Main.toInitial(Path.of("day12-input.txt"));

        assertThat(count).isEqualTo(528250271633772L);

        var end = System.currentTimeMillis();
        System.out.println(String.format("it took %s ms", ( end - start)));
    }
}