package org.krjura.adventofcode.day3;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class Day3MainTest {

    @Test
    public void part1_test1() {
        var main = new Day3Main(Path.of("day3-part1-test-1.txt"));
        main.drawWires();
        // main.printWord();
        int min = main.findClosest();

        assertThat(min).isEqualTo(159);
    }

    @Test
    public void part1_test2() {
        var main = new Day3Main(Path.of("day3-part1-test-2.txt"));
        main.drawWires();
        int min = main.findClosest();

        assertThat(min).isEqualTo(135);
    }

    @Test
    public void part2_test1() {
        var main = new Day3Main(Path.of("day3-part2-test1.txt"));
        main.drawWires();
        int min = main.findWithLowestNumberOfSteps();

        assertThat(min).isEqualTo(610);
    }

    @Test
    public void part2_test2() {
        var main = new Day3Main(Path.of("day3-part2-test2.txt"));
        main.drawWires();
        int min = main.findWithLowestNumberOfSteps();

        assertThat(min).isEqualTo(410);
    }
}