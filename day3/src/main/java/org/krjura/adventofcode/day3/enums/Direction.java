package org.krjura.adventofcode.day3.enums;

public enum Direction {

    R, L, U, D
}
