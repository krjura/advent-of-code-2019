package org.krjura.adventofcode.day3;

import org.krjura.adventofcode.day3.enums.Wire;
import org.krjura.adventofcode.day3.pojo.Coordinate;
import org.krjura.adventofcode.day3.pojo.WireWithSteps;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class Day3Predicates {

    public static Predicate<Map.Entry<Coordinate, List<WireWithSteps>>> noStartCoordinate() {
        return Day3Predicates::hasBothWires;
    }

    public static boolean noStartCoordinate(Map.Entry<Coordinate, List<WireWithSteps>> entry) {
        return entry.getKey().x != 0 && entry.getKey().y != 0;
    }

    public static Predicate<Map.Entry<Coordinate, List<WireWithSteps>>> hasBothWires() {
        return Day3Predicates::hasBothWires;
    }

    public static boolean hasBothWires(Map.Entry<Coordinate, List<WireWithSteps>> entry) {
        boolean hasFirst = false;
        boolean hasSecond = false;

        for(WireWithSteps wire : entry.getValue()) {
            if(wire.wire == Wire.FIRST) {
                hasFirst = true;
                continue;
            }

            if(wire.wire == Wire.SECOND) {
                hasSecond = true;
                continue;
            }
        }

        return hasFirst && hasSecond;
    }
}
