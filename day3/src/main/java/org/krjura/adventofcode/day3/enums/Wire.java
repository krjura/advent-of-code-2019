package org.krjura.adventofcode.day3.enums;

public enum Wire {

    FIRST,
    SECOND
}
