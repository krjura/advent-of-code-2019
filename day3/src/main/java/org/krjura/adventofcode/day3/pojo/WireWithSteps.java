package org.krjura.adventofcode.day3.pojo;

import org.krjura.adventofcode.day3.enums.Wire;

import java.util.Objects;

public class WireWithSteps {

    public final Wire wire;

    public final int step;

    public WireWithSteps(Wire wire, int step) {
        this.wire = Objects.requireNonNull(wire);
        this.step = step;
    }

    public static WireWithSteps of(Wire wire, int step) {
        return new WireWithSteps(wire, step);
    }
}
