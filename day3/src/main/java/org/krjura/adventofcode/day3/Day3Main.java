package org.krjura.adventofcode.day3;

import org.krjura.adventofcode.day3.enums.Direction;
import org.krjura.adventofcode.day3.enums.Wire;
import org.krjura.adventofcode.day3.pojo.Coordinate;
import org.krjura.adventofcode.day3.pojo.WirePath;
import org.krjura.adventofcode.day3.pojo.WirePathInstruction;
import org.krjura.adventofcode.day3.pojo.WireWithSteps;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.krjura.adventofcode.day3.Day3Predicates.hasBothWires;
import static org.krjura.adventofcode.day3.Day3Predicates.noStartCoordinate;
import static org.krjura.adventofcode.day3.Day3Utils.checkOptionalInt;
import static org.krjura.adventofcode.day3.Day3Utils.findWire;
import static org.krjura.adventofcode.day3.WireUtils.loadPaths;

public class Day3Main {

    private final Map<Coordinate, List<WireWithSteps>> world;
    private final List<WirePath> wirePaths;

    public Day3Main(Path path) {
        this.world = new HashMap<>();

        this.wirePaths = loadPaths(path);
    }

    public int findClosest() {
        return checkOptionalInt(this
                .world
                .entrySet()
                .stream()
                .filter(hasBothWires()) // only need elements with both wires
                .filter(noStartCoordinate()) // do not include starting point
                .mapToInt(this::calculateDistanceFromStart)
                .min()
        );
    }

    public int findWithLowestNumberOfSteps() {
        return checkOptionalInt(this
                .world
                .entrySet()
                .stream()
                .filter(hasBothWires()) // only need elements with both wires
                .filter(noStartCoordinate()) // do not include starting point
                .mapToInt(this::calculateStepsStart)
                .min()
        );
    }

    private int calculateStepsStart(Map.Entry<Coordinate, List<WireWithSteps>> entry) {
        WireWithSteps firstWire = findWire(entry.getValue(), Wire.FIRST);
        WireWithSteps secondWire = findWire(entry.getValue(), Wire.SECOND);

        return firstWire.step + secondWire.step;
    }

    private int calculateDistanceFromStart(Map.Entry<Coordinate, List<WireWithSteps>> entry) {
        return Math.abs(entry.getKey().x) + Math.abs(entry.getKey().y);
    }

    public void drawWires() {
        drawWire(Wire.FIRST, wirePaths.get(0));
        drawWire(Wire.SECOND, wirePaths.get(1));
    }

    private void drawWire(Wire wire, WirePath wirePath) {
        var current = new Coordinate(0, 0);
        int totalSteps = 0;

        for(WirePathInstruction instruction : wirePath.getPaths()) {
            var direction = instruction.direction;
            var steps = instruction.steps;

            for(int i = 0; i < steps; i++) {
                current = incrementCoordinate(direction, current);
                totalSteps++;

                this.world.merge(current, singletonList(WireWithSteps.of(wire, totalSteps)), this::mergeLists);
            }
        }
    }

    private List<WireWithSteps> mergeLists(List<WireWithSteps> oldOne, List<WireWithSteps> newOne) {
        List<WireWithSteps> all = new ArrayList<>(oldOne.size() + newOne.size());
        all.addAll(oldOne);
        all.addAll(newOne);

        return all;
    }

    private Coordinate incrementCoordinate(Direction direction, Coordinate current) {
        switch (direction) {
            case L:
                return current.decrementX();
            case R:
                return current.incrementX();
            case U:
                return current.incrementY();
            case D:
                return current.decrementY();
            default:
                throw new IllegalArgumentException("unknown direction " + direction);
        }
    }

    public void printWord() {
        System.out.println("word has " + this.world.size() + " entries");

        int maxX = Integer.MIN_VALUE;
        int minX = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE;

        for(Coordinate coordinate : this.world.keySet()) {
            minX = Math.min(minX, coordinate.x);
            maxX = Math.max(maxX, coordinate.x);

            minY = Math.min(minY, coordinate.y);
            maxY = Math.max(maxY, coordinate.y);
        }

        /*
        System.out.println("---");
        System.out.println(String.format("X: %s, %s", minX, maxX));
        System.out.println(String.format("Y: %s, %s", minY, maxY));

        System.out.println("");
        for(int y =  minY; y <= maxY; y++) {
            System.out.println("");
            for(int x =  minX; x <= maxX; x++) {
                Set<Wire> value = this.world.getOrDefault(new Coordinate(x, y), Collections.emptySet());

                if(value.size() == 0) {
                    System.out.print(String.format(" (%3d, %3d, %s) ", x, y, "WO"));
                } else if(value.size() == 1) {
                    String wire = value.contains(Wire.FIRST) ? "1" : "2";
                    System.out.print(String.format(" (%3d, %3d, %s) ", x, y, "W" + wire));
                } else {
                    System.out.print(String.format(" (%3d, %3d, %s) ", x, y, "WX"));
                }
            }
        }
        */
    }

    public static void main(String[] args) {

        double attempts = 0;
        double sum = 0;

        for(int i = 0; i< 100; i++) {
            var start = System.currentTimeMillis();
            var main = new Day3Main(Path.of("day3-input.txt"));

            main.drawWires();
            int min = main.findWithLowestNumberOfSteps();

            var end = System.currentTimeMillis();

            sum += (double) ( end - start );
            attempts += 1.0d;

            System.out.println(String.format("it took %s ms. Value is %s", (end - start), min));
        }

        System.out.println(String.format("it took %s ms in avg", ( sum / attempts )));
    }
}
