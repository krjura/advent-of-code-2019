package org.krjura.adventofcode.day3;

import org.krjura.adventofcode.day3.enums.Wire;
import org.krjura.adventofcode.day3.pojo.WireWithSteps;

import java.util.List;
import java.util.OptionalInt;

public class Day3Utils {

    public static int checkOptionalInt(OptionalInt val) {
        if(val.isPresent()) {
            return val.getAsInt();
        }  else {
            throw new RuntimeException("no cross path found");
        }
    }

    public static WireWithSteps findWire(List<WireWithSteps> value, Wire type) {

        for(WireWithSteps wire : value) {
            if(wire.wire == type) {
                return wire;
            }
        }

        throw new RuntimeException("cannot find wire of type " + type);
    }
}
