package org.krjura.adventofcode.day3.pojo;

import java.util.List;
import java.util.Objects;

public class WirePath {

    private final List<WirePathInstruction> paths;

    public WirePath(List<WirePathInstruction> paths) {
        this.paths = Objects.requireNonNull(paths);
    }

    public static WirePath of(List<WirePathInstruction> paths) {
        return new WirePath(paths);
    }

    public List<WirePathInstruction> getPaths() {
        return paths;
    }

    @Override
    public String toString() {
        return "WirePath{" +
                "paths=" + paths +
                '}';
    }
}
