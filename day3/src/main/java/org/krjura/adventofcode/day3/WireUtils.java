package org.krjura.adventofcode.day3;

import org.krjura.adventofcode.day3.enums.Direction;
import org.krjura.adventofcode.day3.pojo.WirePath;
import org.krjura.adventofcode.day3.pojo.WirePathInstruction;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WireUtils {

    private WireUtils() {
        // util
    }

    public static List<WirePath> loadPaths(Path path) {
        try {
            return Files
                    .readAllLines(path, StandardCharsets.UTF_8)
                    .stream()
                    .map(WireUtils::toWirePath)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException("unable to read from path " + path, e);
        }
    }

    public static WirePath toWirePath(String instructions) {
        String[] paths = instructions.split(",");

        return WirePath.of(Stream
                .of(paths)
                .map(WireUtils::toWirePathInstruction)
                .collect(Collectors.toList())
        );
    }

    public static WirePathInstruction toWirePathInstruction(String instruction) {
        Direction direction = Direction.valueOf(instruction.substring(0, 1));
        int steps = Integer.parseInt(instruction.substring(1));

        return WirePathInstruction.of(direction, steps);
    }
}
