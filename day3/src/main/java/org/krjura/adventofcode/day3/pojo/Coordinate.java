package org.krjura.adventofcode.day3.pojo;

import java.util.Objects;

public class Coordinate {

    public final int x;

    public final int y;

    public final int hashCode;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
        this.hashCode = Objects.hash(x, y); // cache for performance
    }

    public Coordinate decrementX() {
        return new Coordinate(this.x - 1, this.y);
    }

    public Coordinate incrementX() {
        return new Coordinate(this.x + 1, this.y);
    }

    public Coordinate decrementY() {
        return new Coordinate(this.x, this.y - 1);
    }

    public Coordinate incrementY() {
        return new Coordinate(this.x, this.y + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Coordinate that = (Coordinate) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return this.hashCode;
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}