package org.krjura.adventofcode.day3.pojo;

import org.krjura.adventofcode.day3.enums.Direction;

import java.util.Objects;

public class WirePathInstruction {

    public final Direction direction;

    public final int steps;

    public WirePathInstruction(Direction direction, int steps) {
        this.direction = Objects.requireNonNull(direction);
        this.steps = steps;
    }

    public static WirePathInstruction of(Direction direction, int steps) {
        return new WirePathInstruction(direction, steps);
    }

    @Override
    public String toString() {
        return "WirePathInstruction{" +
                "direction=" + direction +
                ", steps=" + steps +
                '}';
    }
}
