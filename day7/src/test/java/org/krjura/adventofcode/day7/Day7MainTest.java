package org.krjura.adventofcode.day7;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class Day7MainTest {

    @Test
    public void testExample1() {
        var main = new Day7Main(Path.of("day7-part2-example1.txt"));
        var result = main.calculate();

        assertThat(result).isNotNull();
        assertThat(result.getMax()).isEqualTo(139629729);
        assertThat(result.getInput().getInputs()).isEqualTo(List.of(9,8,7,6,5));
    }

    @Test
    public void testExample2() {
        var main = new Day7Main(Path.of("day7-part2-example2.txt"));
        var result = main.calculate();

        assertThat(result).isNotNull();
        assertThat(result.getMax()).isEqualTo(18216);
        assertThat(result.getInput().getInputs()).isEqualTo(List.of(9,7,8,5,6));
    }
}