package org.krjura.adventofcode.day7.providers;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import org.krjura.adventofcode.day5.providers.InputDataProvider;
import org.krjura.adventofcode.day5.providers.OutputDataProvider;

public class DataProvider implements InputDataProvider, OutputDataProvider {

    private final String name;
    private final LinkedBlockingQueue<Integer> queue;

    public DataProvider(String name, List<Integer> initialData) {
        this.name = name;
        this.queue = new LinkedBlockingQueue<>(initialData);
    }

    @Override
    public int data() {
        // System.out.println(format("taking data from %s", name));
        try {
            return queue.take();
        } catch (InterruptedException e) {
            throw new RuntimeException("interrupted", e);
        }
    }

    @Override
    public void put(int data) {
        // System.out.println(format("putting %s into %s", data, name));
        this.queue.add(data);
    }

    public List<Integer> getData() {
        return new ArrayList<>(this.queue);
    }
}
