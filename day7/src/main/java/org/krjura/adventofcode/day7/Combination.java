package org.krjura.adventofcode.day7;

import java.util.List;
import java.util.Objects;

public class Combination {

    private final List<Integer> used;

    private final List<Integer> unused;

    public Combination(List<Integer> used, List<Integer> unused) {
        this.used = Objects.requireNonNull(used);
        this.unused = Objects.requireNonNull(unused);
    }

    public static Combination of(List<Integer> used, List<Integer> unused) {
        return new Combination(used, unused);
    }

    public List<Integer> getUnused() {
        return unused;
    }

    public List<Integer> getUsed() {
        return used;
    }

    @Override
    public String toString() {
        return "Combination{" +
                "used=" + used +
                ", unused=" + unused +
                '}';
    }
}