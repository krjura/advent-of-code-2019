package org.krjura.adventofcode.day7;

import java.util.List;
import java.util.Objects;

public class Input {

    private final List<Integer> inputs;

    public Input(List<Integer> inputs) {
        this.inputs = Objects.requireNonNull(inputs);
    }

    public static Input of(List<Integer> used) {
        return new Input(used);
    }

    public List<Integer> getInputs() {
        return inputs;
    }

    @Override
    public String toString() {
        return "Inputs{" +
                "inputs=" + inputs +
                '}';
    }
}
