package org.krjura.adventofcode.day7;

import java.util.Objects;

public class AmplifierResult {

    private final int max;

    private final Input input;

    public AmplifierResult(int max, Input input) {
        this.max = max;
        this.input = Objects.requireNonNull(input);
    }

    public static AmplifierResult of(int max, Input input) {
        return new AmplifierResult(max, input);
    }

    public Input getInput() {
        return input;
    }

    public int getMax() {
        return max;
    }
}
