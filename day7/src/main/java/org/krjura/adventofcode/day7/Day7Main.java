package org.krjura.adventofcode.day7;

import org.krjura.adventofcode.day5.Day5Main;

import java.nio.file.Path;
import java.util.*;
import org.krjura.adventofcode.day5.providers.InputDataProvider;
import org.krjura.adventofcode.day5.providers.OutputDataProvider;
import org.krjura.adventofcode.day7.providers.DataProvider;

import static java.lang.String.format;

public class Day7Main {

    private final Path inputFile;

    private List<Input> inputs;

    public Day7Main(Path inputFile) {
        this.inputFile = inputFile;
        this.inputs = calculateInputs();
    }

    public AmplifierResult calculate() {

        int max = Integer.MIN_VALUE;
        Input maxInput = null;

        for(Input input : inputs) {

            var amp1Io = new DataProvider("1", List.of(input.getInputs().get(0), 0));
            var amp2Io = new DataProvider("2", List.of(input.getInputs().get(1)));
            var amp3Io = new DataProvider("3", List.of(input.getInputs().get(2)));
            var amp4Io = new DataProvider("4", List.of(input.getInputs().get(3)));
            var amp5Io = new DataProvider("5", List.of(input.getInputs().get(4)));

            var amp1 = new Amplifier("1", this.inputFile, amp1Io, amp2Io);
            var amp2 = new Amplifier("2", this.inputFile, amp2Io, amp3Io);
            var amp3 = new Amplifier("3", this.inputFile, amp3Io, amp4Io);
            var amp4 = new Amplifier("4", this.inputFile, amp4Io, amp5Io);
            var amp5 = new Amplifier("5", this.inputFile, amp5Io, amp1Io);

            startAndWaitCompletion(amp1, amp2, amp3, amp4, amp5);

            if(amp1Io.getData().size() != 1) {
                throw new RuntimeException(
                        format("output data got more results then expected %s in %s",
                                amp2Io.getData(), input
                        )
                );
            }

            int currentValue = amp1Io.getData().get(0);
            if(max < currentValue) {
                max = currentValue;
                maxInput = input;
            }
        }

        return AmplifierResult.of(max, maxInput);
    }

    private void startAndWaitCompletion(Thread... workers) {
        for(Thread worker : workers) {
            worker.start();
        }

        for(Thread worker : workers) {
            try {
                worker.join();
            } catch (InterruptedException e) {
                throw new RuntimeException("interrupted", e);
            }
        }
    }

    private List<Input> calculateInputs() {

        var solutions = new LinkedList<Input>();
        var taskQueue = initTaskQueue();

        while (taskQueue.size() != 0) {
            var combination = taskQueue.poll();

            if(combination.getUnused().size() == 0) {
                solutions.add(Input.of(combination.getUsed()));
                continue;
            }

            for(Integer option : combination.getUnused()) {
                taskQueue.add(addCombination(option, combination));
            }
        }

        return solutions;
    }

    private Combination addCombination(Integer option, Combination combination) {
        return Combination.of(addToList(combination.getUsed(), option), removeFromList(combination.getUnused(), option));
    }

    private List<Integer> removeFromList(List<Integer> list, Integer element) {
        var data = new ArrayList<Integer>(list.size());
        data.addAll(list);
        data.remove(element);

        return Collections.unmodifiableList(data);
    }

    private List<Integer> addToList(List<Integer> list, Integer element) {
        var data = new ArrayList<Integer>(list.size() + 1);
        data.addAll(list);
        data.add(element);

        return Collections.unmodifiableList(data);
    }

    private LinkedList<Combination> initTaskQueue() {
        var queue = new LinkedList<Combination>();
        queue.add(Combination.of(List.of(), List.of(5, 6, 7, 8, 9)));
        return queue;
    }

    private static class Amplifier extends Thread {

        private final Day5Main main;

        public Amplifier(String name, Path inputPath, InputDataProvider input, OutputDataProvider output) {
            this.main = new Day5Main(name, inputPath, input, output);
            this.main.disableLogging();
        }

        @Override
        public void run() {
            this.main.calculate();
        }
    }

    public static void main(String[] args) {
        var main = new Day7Main(Path.of("day7-input.txt"));
        var result = main.calculate();

        System.out.println(format("got result %s for input %s", result.getMax(), result.getInput()));
    }
}
