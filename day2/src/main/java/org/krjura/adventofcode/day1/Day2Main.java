package org.krjura.adventofcode.day1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Day2Main {

    private static final String CONST_ELEMENT_SEPARATOR = ",";

    private int[] positions;
    private final int[] initialPositions;
    private final boolean logStages;

    public Day2Main(Path inputPath, boolean logStages) {
        this.initialPositions = convertToInt(readInputFile(inputPath).split(CONST_ELEMENT_SEPARATOR));
        this.positions = Arrays.copyOf(this.initialPositions, this.initialPositions.length);
        this.logStages = logStages;
    }

    private String readInputFile(Path inputPath) {
        try {
            return validateInput(Files.readAllLines(inputPath, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException("cannot read input file", e);
        }
    }

    private String validateInput(List<String> input) {
        if(input.size() > 1) {
            throw new IllegalArgumentException("got input with lines " + input.size() + " expecting 1");
        }

        return input.get(0);
    }

    public int calculateForPair(int expected) {
        for(int noun = 0; noun <= 99; noun++) {
            for(int verb = 0; verb <= 99; verb++) {
                reinitializePositions();

                this.positions[1] = noun;
                this.positions[2] = verb;

                int[] result = calculate();

                if(result[0] == expected) {
                    System.out.println(
                            String.format("found noun %s and verb %s for expected result %s", noun, verb, expected)
                    );

                    return (100 * noun) + verb;
                }
            }
        }

        throw new IllegalArgumentException("unable to find pair for expected value of " + expected);
    }

    private void reinitializePositions() {
        this.positions = Arrays.copyOf(this.initialPositions, this.initialPositions.length);
    }

    public int[] calculate() {
        if(logStages) {
            System.out.println("starting calculation");
        }

        boolean active = true;
        int position = 0;

        while (active) {
            int opcode = positions[position];

            if(logStages) {
                System.out.println(String.format("opcode %s at position %s", opcode, position));
            }

            switch (opcode) {
                case 1:
                    position = calculateOpcode1(position);
                    break;
                case 2:
                    position = calculateOpcode2(position);
                    break;
                case 99:
                    active = false;
                    break;
                default:
                    throw new IllegalArgumentException("invalid opcode " + opcode);
            }
        }

        return positions;
    }

    private int calculateOpcode2(int position) {
        int firstPosition = positions[position + 1];
        int secondPosition = positions[position + 2];
        int resultPosition = positions[position + 3];

        int firstPositionValue = positions[firstPosition];
        int secondPositionValue = positions[secondPosition];

        int product = firstPositionValue * secondPositionValue;
        positions[resultPosition] = product;

        if(logStages) {
            System.out.println(
                    String.format("[%s: %s] + [%s: %s] = [%s, %s]",
                            firstPosition, firstPositionValue,
                            secondPosition, secondPositionValue,
                            resultPosition, product)
            );
        }

        return position + 4;
    }

    private int calculateOpcode1(int position) {
        int firstPosition = positions[position + 1];
        int secondPosition = positions[position + 2];
        int resultPosition = positions[position + 3];

        int firstPositionValue = positions[firstPosition];
        int secondPositionValue = positions[secondPosition];

        int sum = firstPositionValue + secondPositionValue;
        positions[resultPosition] = sum;

        if(logStages) {
            System.out.println(
                    String.format("[%s: %s] + [%s: %s] = [%s, %s]",
                            firstPosition, firstPositionValue,
                            secondPosition, secondPositionValue,
                            resultPosition, sum)
            );
        }

        return position + 4;
    }

    private int[] convertToInt(String[] positionsAsString) {
        return Stream
                .of(positionsAsString)
                .mapToInt(val -> Integer.parseInt(val.trim()))
                .toArray();
    }

    public static void main(String[] args) {
        /*
        var main = new Day2Main(Path.of("/mnt/storage/kjurasovic/software/workspace-personal/advent-of-code/day2/day2-input.txt"));
        var result = main.calculate();

        System.out.println(Arrays.toString(result));

        System.out.println("position 0: " + result[0]);
         */

        long start = System.currentTimeMillis();

        var main = new Day2Main(
                Path.of("day2-input.txt"),
                false
        );

        System.out.println(main.calculateForPair(19690720));
        // answer to part 2 = 5208

        long end = System.currentTimeMillis();
        System.out.println(String.format("it took %s ms", ( end - start) ));
    }
}
