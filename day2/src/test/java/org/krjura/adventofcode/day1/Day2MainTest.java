package org.krjura.adventofcode.day1;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class Day2MainTest {

    @Test
    public void sequence1() {
        var main = new Day2Main(Path.of("day-2-part1-test-1.txt"));
        var result = main.calculate();

        int[] expected = {2, 0, 0, 0, 99};
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void sequence2() {
        var main = new Day2Main(Path.of("day-2-part1-test-2.txt"));
        var result = main.calculate();

        int[] expected = {2, 3, 0, 6, 99};
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void sequence3() {
        var main = new Day2Main(Path.of("day-2-part1-test-3.txt"));
        var result = main.calculate();

        int[] expected = {2, 4, 4, 5, 99, 9801};
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void sequence4() {
        var main = new Day2Main(Path.of("day-2-part1-test-4.txt"));
        var result = main.calculate();

        int[] expected = {30, 1, 1, 4, 2, 5, 6, 0, 99};
        assertThat(result).isEqualTo(expected);
    }
}