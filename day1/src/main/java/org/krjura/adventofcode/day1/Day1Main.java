package org.krjura.adventofcode.day1;

import java.nio.file.Files;
import java.nio.file.Path;

public class Day1Main {

    public int calculate(Path inputPath) throws Exception {
        return Files
                .readAllLines(inputPath)
                .stream()
                .mapToInt(this::toInt)
                .map(this::calculateWithFuel)
                .sum();
    }

    private Integer calculateWithFuel(Integer val) {
        int fuelAmount = val;
        int totalFuelAmount = 0;

        while(fuelAmount > 0) {
            fuelAmount = calculate(fuelAmount);
            totalFuelAmount += fuelAmount;
        }

        return totalFuelAmount;
    }

    private Integer calculate(Integer val) {
        int result = Math.floorDiv(val, 3) - 2;
        return Math.max(result, 0);
    }

    private Integer toInt(String val) {
        return Integer.parseInt(val);
    }

    public static void main(String[] args) throws Exception {
        int total = new Day1Main()
                .calculate(Path.of("/mnt/storage/kjurasovic/software/workspace-personal/advent-of-code/day-1/input.txt"));

        System.out.println(total);
    }
}
