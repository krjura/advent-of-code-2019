package org.krjura.adventofcode.day1;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class Day1MainTest {

    @Test
    public void calculate14() throws Exception {
        int total = new Day1Main().calculate(Path.of("day-1-part2-test-14.txt"));

        assertThat(total).isEqualTo(2);
    }

    @Test
    public void calculate1969() throws Exception {
        int total = new Day1Main().calculate(Path.of("day-1-part2-test-1969.txt"));

        assertThat(total).isEqualTo(966);
    }

    @Test
    public void calculate100756() throws Exception {
        int total = new Day1Main().calculate(Path.of("day-1-part2-test-100756.txt"));

        assertThat(total).isEqualTo(50346);
    }
}