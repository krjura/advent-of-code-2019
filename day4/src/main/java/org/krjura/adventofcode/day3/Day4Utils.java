package org.krjura.adventofcode.day3;

import java.util.Arrays;
import java.util.Objects;

public class Day4Utils {

    public static boolean hasAdjacent(int[] digits) {
        Objects.requireNonNull(digits);

        for(int i = 1, n = digits.length; i < n;i++) {
            // check adjacent
            int groupSize = groupSize(digits, i);

            if(groupSize == 2){
                return true;
            }
        }

        return false;
    }

    private static int groupSize(int[] digits, int currentIndex) {

        int currentValue = digits[currentIndex];
        int previousAndSame = 0;
        int nextAndSame = 0;

        for(int i = currentIndex + 1, n = digits.length; i < n; i++) {
            int next = digits[i];

            if (currentValue == next) {
                nextAndSame++;
            } else {
                break;
            }
        }

        for(int i = currentIndex - 1; i >= 0; i--) {
            int next = digits[i];

            if (currentValue == next) {
                previousAndSame++;
            } else {
                break;
            }
        }

        return previousAndSame + nextAndSame + 1; // + 1 for current
    }

    public static boolean isNotDecreasing(int[] digits) {
        Objects.requireNonNull(digits);

        for(int i = 1, n = digits.length; i < n; i++) {
            int previous = digits[i-1];
            int current = digits[i];

            if(current < previous) {
                return false;
            }
        }

        return true;
    }

    public static int[] toDigits(int number6) {
        ensureSixDigits(number6);

        int digit6 = number6 / 10_0000;

        int number5 = number6 - (digit6 * 100_000);
        int digit5 = number5 / 10_000;

        int number4 = number5 - (digit5 * 10_000);
        int digit4 = number4 / 1_000;

        int number3 = number4 - (digit4 * 1_000);
        int digit3 = number3 / 100;

        int number2 = number3 - (digit3 * 100);
        int digit2 = number2 / 10;

        int digit1 = number2 % 10;

        return new int[] {digit6, digit5, digit4, digit3, digit2, digit1};
    }

    public static void ensureSixDigits(int number) {
        if(number < 100_000) {
            throw new IllegalArgumentException("not a six digit number -> " + number);
        }
    }

    public static void ensureLarger(int rangeEnd, int rangeStart) {
        if(rangeEnd < rangeStart) {
            throw new IllegalArgumentException(
                    String.format("range end %s must be larger then range start %s", rangeEnd, rangeStart)
            );
        }
    }
}
