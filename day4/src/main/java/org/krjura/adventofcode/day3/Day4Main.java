package org.krjura.adventofcode.day3;

import java.nio.file.Path;
import java.util.stream.IntStream;

import static org.krjura.adventofcode.day3.Day4Utils.ensureLarger;
import static org.krjura.adventofcode.day3.Day4Utils.ensureSixDigits;
import static org.krjura.adventofcode.day3.Day4Utils.hasAdjacent;
import static org.krjura.adventofcode.day3.Day4Utils.isNotDecreasing;
import static org.krjura.adventofcode.day3.Day4Utils.toDigits;

public class Day4Main {

    private final int rangeStart;

    private final int rangeEnd;

    public Day4Main(int rangeStart, int rangeEnd) {
        ensureLarger(rangeEnd, rangeStart); // The value is within the range given in your puzzle input.
        ensureSixDigits(rangeStart); // It is a six-digit number.
        ensureSixDigits(rangeEnd); // It is a six-digit number.

        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
    }

    public long calculate() {
        return IntStream
                .rangeClosed(rangeStart, rangeEnd)
                .filter(Day4Main::doesMatchCriteria)
                .count();
    }

    public static boolean doesMatchCriteria(int i) {
        int[] digits = toDigits(i);

        // Two adjacent digits are the same (like 22 in 122345).
        // Going from left to right, the digits never decrease; they only ever increase or stay the same
        return hasAdjacent(digits) && isNotDecreasing(digits);
    }

    public static void main(String[] args) {
        var start = System.currentTimeMillis();
        var main = new Day4Main(264360, 746325);

        long count = main.calculate();

        var end = System.currentTimeMillis();

        System.out.println(String.format("it took %s ms. Value is %s", (end - start), count));
    }
}
