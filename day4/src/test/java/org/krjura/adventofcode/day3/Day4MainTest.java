package org.krjura.adventofcode.day3;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Day4MainTest {

    @Test
    public void part1_test() {
        assertThat(Day4Main.doesMatchCriteria(112233)).isTrue();
        assertThat(Day4Main.doesMatchCriteria(123444)).isFalse();
        assertThat(Day4Main.doesMatchCriteria(111122)).isTrue();
    }
}