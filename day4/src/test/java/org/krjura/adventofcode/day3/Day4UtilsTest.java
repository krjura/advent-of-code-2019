package org.krjura.adventofcode.day3;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Day4UtilsTest {

    @Test
    public void testHasAdjacent() {
        assertThat(Day4Utils.hasAdjacent( new int[]{1, 2, 3, 4, 5, 6})).isFalse();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 4, 3, 2, 1})).isFalse();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 6, 4, 3, 2, 1})).isTrue();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 5, 3, 2, 1})).isTrue();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 4, 4, 2, 1})).isTrue();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 4, 3, 3, 1})).isTrue();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 4, 3, 2, 2})).isTrue();

        // not part of a group
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 6, 6, 6, 6, 6})).isFalse();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 6, 6, 3, 2, 2})).isTrue(); // last 22 wins
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 6, 6, 3, 3, 1})).isTrue(); // last 33 wins
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 6, 6, 6, 3, 3})).isTrue(); // last 33 wins
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 5, 5, 2, 1})).isFalse();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 5, 5, 2, 2})).isTrue(); // last 22 wins
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 4, 4, 4, 1})).isFalse();
        assertThat(Day4Utils.hasAdjacent( new int[]{6, 5, 4, 3, 3, 3})).isFalse();
        assertThat(Day4Utils.hasAdjacent( new int[]{2, 2, 5, 5, 5, 5})).isTrue(); // first 22 wins
        assertThat(Day4Utils.hasAdjacent( new int[]{2, 2, 1, 5, 5, 5})).isTrue(); // first 22 wins
        assertThat(Day4Utils.hasAdjacent( new int[]{2, 2, 5, 5, 5, 1})).isTrue(); // first 22 wins
    }

    @Test
    public void testIsNotDecreasing() {
        assertThat(Day4Utils.isNotDecreasing( new int[]{1, 1, 1, 1, 1, 1})).isTrue();
        assertThat(Day4Utils.isNotDecreasing( new int[]{1, 2, 3, 4, 5, 6})).isTrue();
        assertThat(Day4Utils.isNotDecreasing( new int[]{1, 2, 2, 3, 4, 5})).isTrue();
        assertThat(Day4Utils.isNotDecreasing( new int[]{1, 2, 3, 3, 4, 5})).isTrue();
        assertThat(Day4Utils.isNotDecreasing( new int[]{1, 2, 3, 4, 4, 6})).isTrue();
        assertThat(Day4Utils.isNotDecreasing( new int[]{1, 2, 3, 4, 5, 5})).isTrue();
        assertThat(Day4Utils.isNotDecreasing( new int[]{2, 1, 2, 2, 2, 2})).isFalse();
        assertThat(Day4Utils.isNotDecreasing( new int[]{2, 2, 1, 2, 2, 2})).isFalse();
        assertThat(Day4Utils.isNotDecreasing( new int[]{2, 2, 2, 1, 2, 2})).isFalse();
        assertThat(Day4Utils.isNotDecreasing( new int[]{2, 2, 2, 2, 1, 2})).isFalse();
        assertThat(Day4Utils.isNotDecreasing( new int[]{2, 2, 2, 2, 2, 1})).isFalse();
    }

    @Test
    public void testToDigits() {
        assertThat(Day4Utils.toDigits(123456)).isEqualTo(new int[] {1, 2, 3, 4, 5, 6});
    }
}