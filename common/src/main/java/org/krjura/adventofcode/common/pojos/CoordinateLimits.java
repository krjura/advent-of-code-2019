package org.krjura.adventofcode.common.pojos;

public class CoordinateLimits {

    public final int minX;
    public final int maxX;
    public final int minY;
    public final int maxY;

    public CoordinateLimits(int minX, int maxX, int minY, int maxY) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
    }

    public static CoordinateLimits of(Iterable<Coordinate> coordinates) {
        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;

        for(Coordinate coordinate : coordinates) {
            minX = Math.min(coordinate.x, minX);
            maxX = Math.max(coordinate.x, maxX);
            minY = Math.min(coordinate.y, minY);
            maxY = Math.max(coordinate.y, maxY);
        }

        return new CoordinateLimits(minX, maxX, minY, maxY);
    }
}