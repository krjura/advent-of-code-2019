package org.krjura.adventofcode.common.pojos;

import java.util.Objects;

public class Coordinate {

    public final int x;

    public final int y;

    private final int hashCode;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
        this.hashCode = Objects.hash(x, y); // cache for performance
    }

    public static Coordinate of(int x, int y) {
        return new Coordinate(x, y);
    }

    public Coordinate decrementX() {
        return new Coordinate(this.x - 1, this.y);
    }

    public Coordinate incrementX() {
        return new Coordinate(this.x + 1, this.y);
    }

    public Coordinate decrementY() {
        return new Coordinate(this.x, this.y - 1);
    }

    public Coordinate incrementY() {
        return new Coordinate(this.x, this.y + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Coordinate that = (Coordinate) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return this.hashCode;
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}