package org.krjura.adventofcode.day8;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class Day8MainTest {

    @Test
    public void testPart2Example() {
        var main = new Day8Main(Path.of("day8-part2-example1.txt"));
        assertThat(main.mergeLayers(2, 2)).isEqualTo("0110");
    }
}