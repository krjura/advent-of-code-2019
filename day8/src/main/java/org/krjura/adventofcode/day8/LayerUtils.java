package org.krjura.adventofcode.day8;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Arrays;

public class LayerUtils {

    private LayerUtils() {
        // utils
    }

    public static ArrayList<Layer> buildLayers(int[] input, int length) {
        var layers = new ArrayList<Layer>(input.length / length);

        for(int i = 0, n = input.length; i < n; i+=length) {
            layers.add(
                    checkLayerSize(Layer.of(Arrays.copyOfRange(input, i, i + length)), length)
            );
        }

        return layers;
    }


    private static Layer checkLayerSize(Layer layer, int length) {
        if(layer.getElements().length != length) {
            throw new RuntimeException(
                    format("layer %s it not of length %s but %s", layer, length, layer.getElements().length)
            );
        }

        return layer;
    }
}
