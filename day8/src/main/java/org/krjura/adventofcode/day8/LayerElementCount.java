package org.krjura.adventofcode.day8;

import java.util.Objects;

public class LayerElementCount {

    private final long count;

    private final Layer layer;

    public LayerElementCount(long count, Layer layer) {
        this.count = count;
        this.layer = Objects.requireNonNull(layer);
    }

    public static LayerElementCount of(long count, Layer layer) {
        return new LayerElementCount(count, layer);
    }

    public long getCount() {
        return count;
    }

    public Layer getLayer() {
        return layer;
    }

    @Override
    public String toString() {
        return "LayerElementCount{" +
                "count=" + count +
                ", layer=" + layer +
                '}';
    }
}
