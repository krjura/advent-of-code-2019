package org.krjura.adventofcode.day8;

import static java.lang.String.format;
import static java.util.Comparator.comparingLong;
import static org.krjura.adventofcode.day8.LayerUtils.buildLayers;

import java.lang.reflect.Array;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public class Day8Main {

    private Path inputPath;

    public Day8Main(Path inputPath) {
        this.inputPath = inputPath;
    }

    public String mergeLayers(int wide, int tall) {
        int[] input = InputUtils.parseInput(this.inputPath);

        int length = wide * tall;

        var layers = buildLayers(input, length);

        int[] image = new int[length];
        Arrays.fill(image, 2); // start with all transparent

        for(Layer layer : layers) {
            for(int i = 0; i < length; i++) {
                int val = image[i];

                // if transparent just add value
                if(val == 2) {
                    image[i] = layer.getElements()[i];
                }
            }
        }

        String imageString = Arrays.stream(image).mapToObj(Integer::toString).collect(Collectors.joining());
        System.out.println("image is:" + imageString);
        System.out.println("---");
        for(int i = 0; i < length; i++) {
            if(i % wide == 0) {
                System.out.println("");
            }

            if(image[i] == 1) {
                System.out.print("X");
            } else {
                System.out.print(" ");
            }
        }
        System.out.println("\n---");

        return imageString;
    }

    public void calculate(int wide, int tall) {
        int[] input = InputUtils.parseInput(this.inputPath);

        int length = wide * tall;

        var layers = buildLayers(input, length);
        var minCount = findWithMinZeros(layers);

        long checksum = count(minCount, 1) * count(minCount, 2);

        System.out.println(format("checksum is %s in layer %s. Layer count is %s", checksum, minCount, layers.size()));
    }

    private LayerElementCount findWithMinZeros(ArrayList<Layer> layers) {
        return layers
                .stream()
                .map(this::counterZeros)
                .min(comparingLong(LayerElementCount::getCount))
                .orElseThrow(() -> new RuntimeException("no min found"));
    }

    private long count(LayerElementCount count, int expected) {
        return Arrays
                .stream(count.getLayer().getElements())
                .filter(val -> val == expected)
                .count();
    }

    private LayerElementCount counterZeros(Layer layer) {
        long count = Arrays
                .stream(layer.getElements())
                .filter(value -> value == 0)
                .count();

        return LayerElementCount.of(count, layer);
    }

    public static void main(String[] args) {

        var main = new Day8Main(Path.of("day8-input.txt"));
        //main.calculate(25, 6);
        main.mergeLayers(25, 6);

        /*
        var main = new Day8Main(Path.of("day8-part2-example1.txt"));
        main.mergeLayers(2, 2);
        */
        // 1677
    }
}
