package org.krjura.adventofcode.day8;

import java.util.Arrays;
import java.util.Objects;

public class Layer {

    private final int[] elements;

    public Layer(int[] elements) {
        this.elements = Objects.requireNonNull(elements);
    }

    public static Layer of(int[] elements) {
        return new Layer(elements);
    }

    public int[] getElements() {
        return elements;
    }

    @Override
    public String toString() {
        return "Layer{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }
}
