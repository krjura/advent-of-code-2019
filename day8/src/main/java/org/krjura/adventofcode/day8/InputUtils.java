package org.krjura.adventofcode.day8;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class InputUtils {

    private InputUtils() {
        // util
    }

    public static int[] parseInput(Path inputPath) {
        String inputString = readInputFile(inputPath);
        char[] inputChars = inputString.toCharArray();

        int[] input = new int[inputChars.length];

        for(int i = 0, n = inputChars.length; i < n; i++) {
            input[i] = Integer.parseInt(new String(new char[] {inputChars[i]}));
        }

        return input;
    }

    private static String readInputFile(Path inputPath) {
        try {
            return validateInput(Files.readAllLines(inputPath, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException("cannot read input file", e);
        }
    }

    private static String validateInput(List<String> input) {
        if(input.size() > 1) {
            throw new IllegalArgumentException("got input with lines " + input.size() + " expecting 1");
        }

        return input.get(0);
    }
}
