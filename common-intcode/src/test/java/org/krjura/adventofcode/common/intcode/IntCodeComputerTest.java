package org.krjura.adventofcode.common.intcode;

import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.common.intcode.providers.impl.ListOutputDataProvider;
import org.krjura.adventofcode.common.intcode.providers.impl.NoInputDataProvider;
import org.krjura.adventofcode.common.intcode.providers.impl.QueueInputDataProvider;
import org.krjura.adventofcode.common.intcode.providers.impl.QueueOutputDataProvider;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class IntCodeComputerTest {

    @Test
    public void testPart1Example1() {
        ListOutputDataProvider output = new ListOutputDataProvider();
        NoInputDataProvider input = new NoInputDataProvider();

        var main = new IntCodeComputer(
                "main",
                Path.of("etc/examples/example1.txt"),
                input,
                output
        );
        main.disableLogging();
        main.calculate();

        var justData = output
                .getData()
                .stream()
                .filter(it -> it.getOpcode() != 99)
                .map(OutputData::getData)
                .collect(Collectors.toList());
        assertThat(justData)
                .isEqualTo(List.of(109L,1L,204L,-1L,1001L,100L,1L,100L,1008L,100L,16L,101L,1006L,101L,0L,99L));
    }

    @Test
    public void testPart1Example2() {
        ListOutputDataProvider output = new ListOutputDataProvider();
        NoInputDataProvider input = new NoInputDataProvider();

        var main = new IntCodeComputer(
                "main",
                Path.of("etc/examples/example2.txt"),
                input,
                output
        );
        main.disableLogging();
        main.calculate();

        assertThat(output.getData()).hasSize(2);
        assertThat(output.getData().get(0).getOpcode()).isEqualTo(4);
        assertThat(output.getData().get(0).getData()).isBetween(1_000_000_000_000_000L, 9_999_999_999_999_999L);
        assertThat(output.getData().get(1).getOpcode()).isEqualTo(99);
        assertThat(output.getData().get(1).getData()).isEqualTo(0L);
    }

    @Test
    public void testPart1Example3() {
        ListOutputDataProvider output = new ListOutputDataProvider();
        NoInputDataProvider input = new NoInputDataProvider();

        var main = new IntCodeComputer(
                "main",
                Path.of("etc/examples/example3.txt"),
                input,
                output
        );
        main.disableLogging();
        main.calculate();

        assertThat(output.getData()).hasSize(2);
        assertThat(output.getData().get(0).getOpcode()).isEqualTo(4);
        assertThat(output.getData().get(0).getData()).isEqualTo(1125899906842624L);
        assertThat(output.getData().get(1).getOpcode()).isEqualTo(99);
        assertThat(output.getData().get(1).getData()).isEqualTo(0L);
    }
}