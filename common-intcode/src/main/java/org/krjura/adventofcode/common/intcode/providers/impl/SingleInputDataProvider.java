package org.krjura.adventofcode.common.intcode.providers.impl;

import org.krjura.adventofcode.common.intcode.providers.InputDataProvider;

public class SingleInputDataProvider implements InputDataProvider {

    private long data;

    public SingleInputDataProvider(long data) {
        this.data = data;
    }

    @Override
    public long data() {
        return data;
    }
}
