package org.krjura.adventofcode.common.intcode;

import org.krjura.adventofcode.common.intcode.providers.InputDataProvider;
import org.krjura.adventofcode.common.intcode.providers.OutputDataProvider;

import java.nio.file.Path;

public class AsyncIntCodeComputer extends Thread {

    private final IntCodeComputer computer;

    private boolean computerDone = false;

    public AsyncIntCodeComputer(
            String name,
            Path inputPath,
            InputDataProvider input,
            OutputDataProvider output) {

        super(name);
        this.computer = new IntCodeComputer(name, inputPath, input, output);
    }

    @Override
    public void run() {
        this.computer.disableLogging();
        this.computer.calculate();
        this.computerDone = true;
    }

    public boolean isComputerDone() {
        return computerDone;
    }
}
