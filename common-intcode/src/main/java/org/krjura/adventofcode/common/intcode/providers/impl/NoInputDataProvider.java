package org.krjura.adventofcode.common.intcode.providers.impl;

import org.krjura.adventofcode.common.intcode.providers.InputDataProvider;

public class NoInputDataProvider implements InputDataProvider {

    @Override
    public long data() {
        throw new RuntimeException("no data available");
    }
}
