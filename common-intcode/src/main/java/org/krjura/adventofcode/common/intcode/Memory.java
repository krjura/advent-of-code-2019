package org.krjura.adventofcode.common.intcode;

import java.util.HashMap;
import java.util.Map;

public class Memory {

    private Map<Long, Long> data;

    private int relativeBase = 0;

    public Memory(long[] data) {
        this.data = initializeMemory(data);
    }

    private Map<Long, Long> initializeMemory(long[] initial) {
        Map<Long, Long> memory = new HashMap<>(initial.length);

        for(int i = 0, n = initial.length; i < n; i++) {
            memory.put(Integer.toUnsignedLong(i), initial[i]);
        }

        return memory;
    }

    public long get(long position) {
        if(position < 0) {
            throw new RuntimeException("invalid memory position " + position);
        }

        return this.data.getOrDefault(position, 0L);
    }

    public long getWithRelativeBase(long position) {
        long realPosition = this.relativeBase + position;

        if(realPosition < 0) {
            throw new RuntimeException("invalid memory position " + position);
        }

        return this.data.getOrDefault(realPosition, 0L);
    }

    public void set(long position, long val) {
        this.data.put(position, val);
    }

    public Map<Long, Long> getData() {
        return data;
    }

    public int getRelativeBase() {
        return relativeBase;
    }

    public void addToRelativeBase(int val) {
        this.relativeBase += val;
    }
}
