package org.krjura.adventofcode.common.intcode.providers.impl;

import org.krjura.adventofcode.common.intcode.OutputData;
import org.krjura.adventofcode.common.intcode.providers.OutputDataProvider;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class QueueOutputDataProvider implements OutputDataProvider {

    private final LinkedBlockingQueue<OutputData> queue;

    public QueueOutputDataProvider() {
        this.queue = new LinkedBlockingQueue<>();
    }

    public static QueueOutputDataProvider of() {
        return new QueueOutputDataProvider();
    }

    @Override
    public void put(OutputData data) {
        this.queue.add(data);
    }

    public OutputData get() {
        try {
            return this.queue.take();
        } catch (InterruptedException e) {
            throw new RuntimeException("interrupted", e);
        }
    }

    public Queue<OutputData> getData() {
        return queue;
    }

    public void clear() {
        queue.clear();
    }
}
