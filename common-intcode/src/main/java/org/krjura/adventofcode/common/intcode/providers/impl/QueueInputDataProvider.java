package org.krjura.adventofcode.common.intcode.providers.impl;

import org.krjura.adventofcode.common.intcode.providers.InputDataProvider;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueInputDataProvider implements InputDataProvider {

    private final LinkedBlockingQueue<Long> queue;

    public QueueInputDataProvider(List<Long> initialData) {
        this.queue = new LinkedBlockingQueue<>(initialData);
    }

    public static QueueInputDataProvider of() {
        return new QueueInputDataProvider(List.of());
    }

    @Override
    public long data() {
        try {
            return queue.take();
        } catch (InterruptedException e) {
            throw new RuntimeException("interrupted", e);
        }
    }

    public void put(long data) {
        this.queue.add(data);
    }
}
