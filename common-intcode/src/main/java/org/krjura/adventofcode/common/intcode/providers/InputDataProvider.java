package org.krjura.adventofcode.common.intcode.providers;

public interface InputDataProvider {

    long data();
}
