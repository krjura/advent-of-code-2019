package org.krjura.adventofcode.common.intcode;

public class OutputData {

    private final int opcode;

    private final long data;

    public OutputData(int opcode, long data) {
        this.opcode = opcode;
        this.data = data;
    }

    public static OutputData of(int opcode, long data) {
        return new OutputData(opcode, data);
    }

    public int getOpcode() {
        return opcode;
    }

    public long getData() {
        return data;
    }

    @Override
    public String toString() {
        return "OutputData{" +
                "opcode=" + opcode +
                ", data=" + data +
                '}';
    }
}
