package org.krjura.adventofcode.common.intcode.providers.impl;

import org.krjura.adventofcode.common.intcode.OutputData;
import org.krjura.adventofcode.common.intcode.providers.OutputDataProvider;

import java.util.LinkedList;
import java.util.List;

public class ListOutputDataProvider implements OutputDataProvider {

    private List<OutputData> data;

    public ListOutputDataProvider() {
        this.data = new LinkedList<>();
    }

    @Override
    public void put(OutputData data) {
        this.data.add(data);
    }

    public List<OutputData> getData() {
        return data;
    }

    public void clear() {
        data.clear();;
    }
}
