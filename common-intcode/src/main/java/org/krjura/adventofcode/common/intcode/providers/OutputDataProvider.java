package org.krjura.adventofcode.common.intcode.providers;

import org.krjura.adventofcode.common.intcode.OutputData;

public interface OutputDataProvider {

    void put(OutputData data);
}
