package org.krjura.adventofcode.day5.providers;

import java.util.LinkedList;
import java.util.List;

public class ListOutputDataProvider implements OutputDataProvider {

    private List<Integer> data;

    public ListOutputDataProvider() {
        this.data = new LinkedList<>();
    }

    @Override
    public void put(int data) {
        this.data.add(data);
    }

    public List<Integer> getData() {
        return data;
    }

    public void clear() {
        data.clear();;
    }
}
