package org.krjura.adventofcode.day5.providers;

public class SingleInputDataProvider implements InputDataProvider {

    private int data;

    public SingleInputDataProvider(int data) {
        this.data = data;
    }

    @Override
    public int data() {
        return data;
    }
}
