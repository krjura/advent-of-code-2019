package org.krjura.adventofcode.day5;

public class ParameterValue {

    private final int parameter;
    private final int position;
    private final int mode;
    private final int value;
    private final int valueAtPosition;

    private ParameterValue(int parameter, int position, int mode, int value, int valueAtPosition) {
        this.parameter = parameter;
        this.position = position;
        this.mode = mode;
        this.value = value;
        this.valueAtPosition = valueAtPosition;
    }

    private ParameterValue(Builder builder) {
        this(
                builder.parameter,
                builder.position,
                builder.mode,
                builder.value,
                builder.valueAtPosition
        );
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public int getParameter() {
        return parameter;
    }

    public int getPosition() {
        return position;
    }

    public int getMode() {
        return mode;
    }

    public int getValue() {
        return value;
    }

    public int getValueAtPosition() {
        return valueAtPosition;
    }

    public static final class Builder {
        private int parameter;
        private int position;
        private int mode;
        private int value;
        private int valueAtPosition;

        private Builder() {
        }

        public Builder parameter(int val) {
            parameter = val;
            return this;
        }

        public Builder position(int val) {
            position = val;
            return this;
        }

        public Builder mode(int val) {
            mode = val;
            return this;
        }

        public Builder value(int val) {
            value = val;
            return this;
        }

        public Builder valueAtPosition(int val) {
            valueAtPosition = val;
            return this;
        }

        public ParameterValue build() {
            return new ParameterValue(this);
        }
    }
}
