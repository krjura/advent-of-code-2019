package org.krjura.adventofcode.day5;

import static java.lang.String.format;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import org.krjura.adventofcode.day5.providers.InputDataProvider;
import org.krjura.adventofcode.day5.providers.ListOutputDataProvider;
import org.krjura.adventofcode.day5.providers.OutputDataProvider;
import org.krjura.adventofcode.day5.providers.SingleInputDataProvider;

public class Day5Main {

    private static final String CONST_ELEMENT_SEPARATOR = ",";

    private final String name;
    private final InputDataProvider inputDataProvider;
    private final OutputDataProvider outputDataProvider;

    private int[] positions;
    private boolean logStages = true;

    public Day5Main(
            String name,
            Path inputPath,
            InputDataProvider inputDataProvider,
            OutputDataProvider outputDataProvider ) {

        this.positions = convertToInt(readInputFile(inputPath).split(CONST_ELEMENT_SEPARATOR));

        this.name = name;
        this.inputDataProvider = Objects.requireNonNull(inputDataProvider);
        this.outputDataProvider = Objects.requireNonNull(outputDataProvider);
    }

    public void disableLogging() {
        this.logStages = false;
    }

    private String readInputFile(Path inputPath) {
        try {
            return validateInput(Files.readAllLines(inputPath, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException("cannot read input file", e);
        }
    }

    private String validateInput(List<String> input) {
        if(input.size() > 1) {
            throw new IllegalArgumentException("got input with lines " + input.size() + " expecting 1");
        }

        return input.get(0);
    }

    public int[] calculate() {
        if(logStages) {
            System.out.println(format("[%s] starting calculation", name));
        }

        boolean active = true;
        int position = 0;

        while (active) {
            var instruction = OpcodeInstruction.of(positions[position]);
            var opcode = instruction.getOpcode();

            if(logStages) {
                System.out.println(format(
                        "[%s] opcode %s at position %s in instruction %s",
                        name, opcode, position, Arrays.toString(instruction.getInstructions())));
            }

            switch (opcode) {
                case 1:
                    position = calculateOpcode1(position, instruction);
                    break;
                case 2:
                    position = calculateOpcode2(position, instruction);
                    break;
                case 3:
                    position = calculateOpcode3(position, instruction);
                    break;
                case 4:
                    position = calculateOpcode4(position, instruction);
                    break;
                case 5:
                    position = calculateOpcode5(position, instruction);
                    break;
                case 6:
                    position = calculateOpcode6(position, instruction);
                    break;
                case 7:
                    position = calculateOpcode7(position, instruction);
                    break;
                case 8:
                    position = calculateOpcode8(position, instruction);
                    break;
                case 99:
                    active = false;
                    break;
                default:
                    throw new IllegalArgumentException("invalid opcode " + opcode);
            }
        }

        return positions;
    }

    private int calculateOpcode5(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(positions, position, 1);
        var secondVal = instruction.getParameterValue(positions, position, 2);

        if(firstVal.getValue() != 0) {
            return secondVal.getValue();
        }

        return position + 3;
    }

    private int calculateOpcode6(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(positions, position, 1);
        var secondVal = instruction.getParameterValue(positions, position, 2);

        if(firstVal.getValue() == 0) {
            return secondVal.getValue();
        }

        return position + 3;
    }

    private int calculateOpcode7(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(positions, position, 1);
        var secondVal = instruction.getParameterValue(positions, position, 2);
        var resultVal = instruction.getParameterValueWithoutMode(positions, position, 3);

        if(firstVal.getValue() < secondVal.getValue()) {
            positions[resultVal.getValue()] = 1;
        } else {
            positions[resultVal.getValue()] = 0;
        }

        return position + 4;
    }

    private int calculateOpcode8(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(positions, position, 1);
        var secondVal = instruction.getParameterValue(positions, position, 2);
        var resultVal = instruction.getParameterValueWithoutMode(positions, position, 3);

        if(firstVal.getValue() == secondVal.getValue()) {
            positions[resultVal.getValue()] = 1;
        } else {
            positions[resultVal.getValue()] = 0;
        }

        return position + 4;
    }

    private int calculateOpcode4(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(positions, position, 1);

        if(logStages) {
            System.out.println(format(
                    "[%s] opcode 4: [%s, %s = %s]",
                    this.name, firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue()));
        }

        outputDataProvider.put(firstVal.getValue());

        return position + 2;
    }

    private int calculateOpcode3(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValueWithoutMode(positions, position, 1);

        int input =  inputDataProvider.data();
        positions[firstVal.getValue()] = input;

        if(logStages) {
            System.out.println(format(
                    "[%s] opcode 3 :[%s: %s = %s]",
                    this.name, firstVal.getPosition(), firstVal.getValueAtPosition(), input));
        }

        return position + 2;
    }

    private int calculateOpcode2(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(positions, position, 1);
        var secondVal = instruction.getParameterValue(positions, position, 2);
        var resultVal = instruction.getParameterValueWithoutMode(positions, position, 3);

        int product = firstVal.getValue() * secondVal.getValue();
        positions[resultVal.getValue()] = product;

        if(logStages) {
            System.out.println(
                    format("[%s] [%s: %s - %s] * [%s: %s - %s] = [%s, %s = %s]",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue(),
                            resultVal.getPosition(), resultVal.getValue(), product)
            );
        }

        return position + 4;
    }

    private int calculateOpcode1(int position, OpcodeInstruction instruction) {
        var firstVal = instruction.getParameterValue(positions, position, 1);
        var secondVal = instruction.getParameterValue(positions, position, 2);
        var resultVal = instruction.getParameterValueWithoutMode(positions, position, 3);

        int sum = firstVal.getValue() + secondVal.getValue();
        positions[resultVal.getValue()] = sum;

        if(logStages) {
            System.out.println(
                    format("[%s] [%s: %s - %s] + [%s: %s - %s] = [%s, %s = %s]",
                            this.name,
                            firstVal.getPosition(), firstVal.getValueAtPosition(), firstVal.getValue(),
                            secondVal.getPosition(), secondVal.getValueAtPosition(), secondVal.getValue(),
                            resultVal.getPosition(), resultVal.getValue(), sum)
            );
        }

        return position + 4;
    }

    private int[] convertToInt(String[] positionsAsString) {
        return Stream
                .of(positionsAsString)
                .mapToInt(val -> Integer.parseInt(val.trim()))
                .toArray();
    }

    public static void main(String[] args) {
        var main = new Day5Main(
                "main",
                Path.of("day5-input.txt"),
                new SingleInputDataProvider(5),
                new ListOutputDataProvider()
        );
        main.disableLogging();
        main.calculate();
    }
}
