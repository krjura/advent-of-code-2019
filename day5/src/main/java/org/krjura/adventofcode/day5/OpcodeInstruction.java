package org.krjura.adventofcode.day5;

public class OpcodeInstruction {

    private int[] instructions;

    private OpcodeInstruction(int optCodeInstructions) {
        this.instructions = processOpcodeInstruction(optCodeInstructions);
    }

    public static OpcodeInstruction of(int optCodeInstructions) {
        return new OpcodeInstruction(optCodeInstructions);
    }

    private int[] processOpcodeInstruction(int opcode) {
        char[] instructionsChars = Integer.toString(opcode).toCharArray();
        int[] instructionsInts = new int[instructionsChars.length];

        for(int i = 0, n = instructionsInts.length; i < n; i++) {
            instructionsInts[i] = Integer.parseInt(Character.toString(instructionsChars[i]));
        }

        return instructionsInts;
    }

    public int[] getInstructions() {
        return instructions;
    }

    public int getOpcode() {
        return getValueFromBack(1) * 10 + getValueFromBack(0);
    }

    public int getMode(int parameter) {
        if(parameter < 1) {
            throw new IllegalArgumentException("mode parameter must be > 1. Got " + parameter);
        }

        // +2 for opcode, (parameter -1) since we want parameter number to start from 1
        return getValueFromBack(2 + ( parameter - 1));
    }

    private int getValueFromBack(int position) {
        int index = this.instructions.length - position - 1; // since array start with 0

        if(index < 0) {
            return 0;
        } else if(index >= this.instructions.length ) {
            return 0;
        } else {
            return this.instructions[index];
        }
    }

    public ParameterValue getParameterValue(int[] positions, int position, int parameter) {
        int paramPosition = position + parameter;
        int value = positions[paramPosition];
        int realValue = 0;

        int mode = getMode(parameter);
        switch (mode) {
            case 0: // position mode
                realValue = positions[value];
                break;
            case 1: // immediate mode
                realValue = value;
                break;
            default:
                throw new IllegalArgumentException("unknown mode " + mode);
        }

        return ParameterValue
                .newBuilder()
                .parameter(parameter)
                .position(paramPosition)
                .mode(mode)
                .value(realValue)
                .valueAtPosition(value)
                .build();
    }

    public ParameterValue getParameterValueWithoutMode(int[] positions, int position, int parameter) {
        int paramPosition = position + parameter;
        int value = positions[paramPosition];

        return ParameterValue
                .newBuilder()
                .parameter(parameter)
                .position(paramPosition)
                .mode(0)
                .value(value)
                .valueAtPosition(value)
                .build();
    }
}
