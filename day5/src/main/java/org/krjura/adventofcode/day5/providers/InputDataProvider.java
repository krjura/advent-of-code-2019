package org.krjura.adventofcode.day5.providers;

public interface InputDataProvider {

    int data();
}
