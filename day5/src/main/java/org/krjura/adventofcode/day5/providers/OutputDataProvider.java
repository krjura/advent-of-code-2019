package org.krjura.adventofcode.day5.providers;

public interface OutputDataProvider {

    void put(int data);
}
