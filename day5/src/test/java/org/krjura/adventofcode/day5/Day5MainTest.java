package org.krjura.adventofcode.day5;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.krjura.adventofcode.day5.providers.ListOutputDataProvider;
import org.krjura.adventofcode.day5.providers.SingleInputDataProvider;

public class Day5MainTest {

    @Test
    public void testLessThen8() {
        ListOutputDataProvider odp = new ListOutputDataProvider();

        var main = new Day5Main(Path.of("day5-part2-example.txt"), new SingleInputDataProvider(1), odp);
        main.calculate();
        assertThat(odp.getData()).hasSize(1);
        assertThat(odp.getData().get(0)).isEqualTo(999);

        odp.clear();
        main = new Day5Main(Path.of("day5-part2-example.txt"), new SingleInputDataProvider(5), odp);
        main.calculate();
        assertThat(odp.getData()).hasSize(1);
        assertThat(odp.getData().get(0)).isEqualTo(999);

        odp.clear();
        main = new Day5Main(Path.of("day5-part2-example.txt"), new SingleInputDataProvider(5), odp);
        main.calculate();
        assertThat(odp.getData()).hasSize(1);
        assertThat(odp.getData().get(0)).isEqualTo(999);
    }

    @Test
    public void testEqualTo8() {
        ListOutputDataProvider odp = new ListOutputDataProvider();

        var main = new Day5Main(Path.of("day5-part2-example.txt"), new SingleInputDataProvider(8), odp);
        main.calculate();
        assertThat(odp.getData()).hasSize(1);
        assertThat(odp.getData().get(0)).isEqualTo(1000);
    }

    @Test
    public void testLargerThen8() {
        ListOutputDataProvider odp = new ListOutputDataProvider();

        var main = new Day5Main(Path.of("day5-part2-example.txt"), new SingleInputDataProvider(9), odp);
        main.calculate();
        assertThat(odp.getData()).hasSize(1);
        assertThat(odp.getData().get(0)).isEqualTo(1001);

        odp.clear();
        main = new Day5Main(Path.of("day5-part2-example.txt"), new SingleInputDataProvider(15), odp);
        main.calculate();
        assertThat(odp.getData()).hasSize(1);
        assertThat(odp.getData().get(0)).isEqualTo(1001);

        odp.clear();
        main = new Day5Main(Path.of("day5-part2-example.txt"), new SingleInputDataProvider(70), odp);
        main.calculate();
        assertThat(odp.getData()).hasSize(1);
        assertThat(odp.getData().get(0)).isEqualTo(1001);
    }
}